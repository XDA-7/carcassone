/*using System;
using System.Collections.Generic;
using Carcassone.Bots;
using Carcassone.Engine;
using Xunit;

namespace Carcassone.Engine.Tests
{
    public class DeepCopy
    {
        [Fact]
        public void SessionStatsMatch()
        {
            var originalSession = GenerateKnownSession();
            var copySession = Session.Clone(originalSession);

            AssertLandmarkStatsMatch(originalSession.Cities, copySession.Cities);
            AssertLandmarkStatsMatch(originalSession.Roads, copySession.Roads);
            AssertFieldsCityCountMatches(originalSession.Fields, copySession.Fields);
            AssertLandmarkStatsMatch(originalSession.Fields, copySession.Fields);
            AssertLandmarkStatsMatch(originalSession.Chapels, copySession.Chapels);
            AssertLandmarkStatsMatch(originalSession.Gardens, copySession.Gardens);
        }

        private void AssertLandmarkStatsMatch<T>(IEnumerable<T> originalLandmarksGenerator, IEnumerable<T> clonedLandmarksGenerator) where T : Landmark
        {
            var originalLandmarks = new List<T>(originalLandmarksGenerator);
            var clonedLandmarks = new List<T>(clonedLandmarksGenerator);
            
            Assert.Equal(originalLandmarks.Count, clonedLandmarks.Count);
            T matchingLandmark = null;
            foreach (var originalLandmark in originalLandmarks)
            {    
                foreach (var clonedLandmark in clonedLandmarks)
                {
                    if (originalLandmark.Score == clonedLandmark.Score)
                    {
                        matchingLandmark = clonedLandmark;
                        break;
                    }
                }

                Assert.NotNull(matchingLandmark);
                clonedLandmarks.Remove(matchingLandmark);
                matchingLandmark = null;
            }

            Assert.Empty(clonedLandmarks);
        }

        private void AssertFieldsCityCountMatches(List<Field> originalFields, List<Field> clonedFields)
        {
            Field matchingField = null;
            foreach (var originalField in originalFields)
            {
                foreach (var clonedField in clonedFields)
                {
                    if (originalField.Cities.Length == clonedField.Cities.Length)
                    {
                        matchingField = clonedField;
                        break;
                    }
                }

                Assert.NotNull(matchingField);
                clonedFields.Remove(matchingField);
                matchingField = null;
            }

            Assert.Empty(clonedFields);
        }

        [Fact]
        public void CopiedSessionSharesNoReferences()
        {
            var builder = new RandomBuilder();
            builder.BuildToEnd();
            var originalSession = builder.Session;
            var copySession = Session.Clone(originalSession);

            for (var i = 0; i < originalSession._map.GetLength(0); i++)
            {
                for (var j = 0; j < originalSession._map.GetLength(1); j++)
                {
                    if (originalSession._map[i,j] != null)
                    {
                        Assert.NotEqual(originalSession._map[i,j], copySession._map[i,j]);
                        // Also check chapel map and garden map
                        AssertParallelTilesDontShareLandmarks(originalSession._map[i,j], copySession._map[i,j]);
                    }
                    if (originalSession.ChapelMap[i,j] != null)
                    {
                        Assert.NotEqual(originalSession.ChapelMap[i,j], copySession.ChapelMap[i,j]);
                    }
                    if (originalSession.GardenMap[i,j] != null)
                    {
                        Assert.NotEqual(originalSession.GardenMap[i,j], copySession.GardenMap[i,j]);
                    }
                }
            }

            foreach(var originalLandmark in originalSession.GetLandmarks())
            {
                foreach (var duplicateLandMark in copySession.GetLandmarks())
                {
                    AssertLandmarksDontShareReferences(originalLandmark, duplicateLandMark);
                }
            }

            foreach (var player in originalSession._players)
            {
                Assert.DoesNotContain(player, copySession._players);
            }
        }

        private void AssertParallelTilesDontShareLandmarks(Tile original, Tile duplicate)
        {
            if (original != null && duplicate != null)
            {
                for (var i = 0; i < 4; i++)
                {
                    var originalLandmark = original.Edges[i].Landmark;
                    var duplicateLandmark = duplicate.Edges[i].Landmark;
                    AssertLandmarkStatsEqual(originalLandmark, duplicateLandmark);

                    var originalField = original.Corners[i].Field;
                    var duplicateField = duplicate.Corners[i].Field;
                    AssertFieldsStatsEqual(originalField, duplicateField);
                }
            }
            else
            {
                Assert.Null(original);
                Assert.Null(duplicate);
            }
        }

        private void AssertLandmarkStatsEqual(Landmark original, Landmark duplicate)
        {
            if (original != null && duplicate != null)
            {
                Assert.NotEqual(original, duplicate);
                Assert.Equal(original.GetType(), duplicate.GetType());
                Assert.Equal(original.Score, duplicate.Score);
            }
            else
            {
                Assert.Null(original);
                Assert.Null(duplicate);
            }
        }

        private void AssertFieldsStatsEqual(Field original, Field duplicate)
        {
            if (original != null && duplicate != null)
            {
                Assert.NotEqual(original, duplicate);
                Assert.Equal(original.Score, duplicate.Score);
                Assert.Equal(original.Cities.Length, duplicate.Cities.Length);
                Assert.Equal(original.TileCorners.Count, duplicate.TileCorners.Count);
                Assert.Equal(original._claimants.Count, duplicate._claimants.Count);
            }
            else
            {
                Assert.Null(original);
                Assert.Null(duplicate);
            }
        }

        private void AssertLandmarksDontShareReferences(Landmark original, Landmark duplicate)
        {
            if (original.GetType() != duplicate.GetType())
            {
                return;
            }

            Assert.NotEqual(original, duplicate);
            if (original is VaryingTileLandmark)
            {
                AssertVaryingTileLandmarksDontShareReferences(original as VaryingTileLandmark, duplicate as VaryingTileLandmark);
            }

            if (original is Field)
            {
                AssertFieldsDontShareReferences(original as Field, duplicate as Field);
            }

            foreach (var originalMeeple in original._claimants)
            {
                Assert.DoesNotContain(originalMeeple.Key, duplicate._claimants.Keys);
            }
        }

        private void AssertVaryingTileLandmarksDontShareReferences(VaryingTileLandmark original, VaryingTileLandmark duplicate)
        {
            foreach (var originalTile in original._tiles)
            {
                Assert.DoesNotContain(originalTile, duplicate._tiles);
            }
        }

        private void AssertFieldsDontShareReferences(Field original, Field duplicate)
        {
            foreach (var originalCity in original.Cities)
            {
                foreach (var duplicateCity in duplicate.Cities)
                {
                    Assert.NotEqual(originalCity, duplicateCity);
                }
            }
        }

        private Session GenerateKnownSession()
        {
            var session = Session.New();
            session.PlaceTile(TileLayouts.CurvedRoad.Rotate(3), 31, 30);
            session.EndTurn();

            session.PlaceTile(TileLayouts.TSection.Rotate(2), 31, 29);
            session.EndTurn();

            session.PlaceTile(TileLayouts.StraightRoadCity.Rotate(2), 30, 29);
            session.EndTurn();

            session.PlaceTile(TileLayouts.CityBridge.Rotate(1), 30, 31);
            session.EndTurn();

            session.PlaceTile(TileLayouts.QuestionMarkRight, 31, 31);
            session.EndTurn();

            session.PlaceTile(TileLayouts.OppositeWalls.Rotate(1), 31, 32);
            session.EndTurn();

            session.PlaceTile(TileLayouts.StraightRoadCity.Rotate(3), 31, 28);
            session.EndTurn();

            session.PlaceTile(TileLayouts.DoubleCity, 30, 28);
            session.EndTurn();

            session.PlaceTile(TileLayouts.ChapelRoad.Rotate(3), 29, 29);
            session.EndTurn();

            session.PlaceTile(TileLayouts.CurvedRoad, 29, 30);
            session.EndTurn();

            session.PlaceTile(TileLayouts.OppositeWalls.Rotate(1), 30, 32);
            session.EndTurn();

            session.PlaceTile(TileLayouts.SingleCity.Rotate(2), 30, 33);
            session.EndTurn();

            return session;
        }

        private Session GenerateRandomSession()
        {
            var builder = new RandomBuilder();
            builder.BuildToEnd();
            return builder.Session;
        }
    }
}
*/