/*using System;
using Carcassone.Bots;
using Carcassone.Engine;
using Xunit;

namespace Carcassone.Engine.Tests
{
    public class GameLogic
    {
        [Fact]
        public void Test01()
        {
            var session = Session.New();
            var claimableLandmark = session.PlaceTile(TileLayouts.SingleCity.Rotate(2), 30, 31);
            session.EndTurn();
            Assert.Equal(1, claimableLandmark.Cities.Count);
            Assert.Equal(1, claimableLandmark.Fields.Length);
            Assert.True(claimableLandmark.Cities[0].IsComplete());
            Assert.Equal(4, claimableLandmark.Cities[0].Score);
            Assert.Equal(3, claimableLandmark.Fields[0].Score);

            claimableLandmark = session.PlaceTile(TileLayouts.AllRoad, 29, 30);
            session.EndTurn();
            Assert.Equal(4, claimableLandmark.Roads.Count);
            Assert.Equal(4, claimableLandmark.Fields.Length);
            Assert.Empty(claimableLandmark.Cities);
            Assert.Single(claimableLandmark.Fields, field => field.Score == 3);
            var fourRoadFields = claimableLandmark.Fields;

            claimableLandmark = session.PlaceTile(TileLayouts.DoubleCityRoad.Rotate(2), 29, 29);
            session.EndTurn();
            Assert.Equal(2, claimableLandmark.Fields.Length);
            Assert.Equal(1, claimableLandmark.Cities.Count);
            Assert.Equal(1, claimableLandmark.Roads.Count);
            Assert.Contains(claimableLandmark.Fields[0], fourRoadFields);
            Assert.Contains(claimableLandmark.Fields[1], fourRoadFields);
            Assert.Equal(1, claimableLandmark.Cities[0].Score);
            Assert.Equal(2, claimableLandmark.Roads[0].Score);

            claimableLandmark = session.PlaceTile(TileLayouts.CityBridge, 28, 29);
            session.EndTurn();
            Assert.Equal(2, claimableLandmark.Cities[0].Score);

            claimableLandmark = session.PlaceTile(TileLayouts.ChapelRoad.Rotate(1), 31, 30);
            session.EndTurn();
            Assert.True(claimableLandmark.Roads[0].IsComplete());
            Assert.Equal(3, claimableLandmark.Roads[0].Score);
            Assert.Equal(1, claimableLandmark.Fields.Length);
            Assert.Equal(3, claimableLandmark.Fields[0].Score);
        }

        // Test merging cities
        [Fact]
        public void Test02() 
        {
            var session = Session.New(2);
            var playerOne = session._players[0];
            var playerTwo = session._players[1];

            session.PlaceTile(TileLayouts.TSection, 31, 30);
            session.EndTurn();

            session.PlaceTile(TileLayouts.CityBridge, 31, 29);
            session.EndTurn();

            session.PlaceTile(TileLayouts.StraightRoad, 29, 30);
            session.EndTurn();

            var claimableLandmarks = session.PlaceTile(TileLayouts.CityBridge, 29, 31);
            claimableLandmarks.Cities[0].Claim(playerOne);
            var playerOneCity = claimableLandmarks.Cities[0];
            session.EndTurn();

            claimableLandmarks = session.PlaceTile(TileLayouts.TSectionCity.Rotate(3), 31, 31);
            claimableLandmarks.Cities[0].Claim(playerTwo);
            var playerTwoCity = claimableLandmarks.Cities[0];
            session.EndTurn();

            Assert.Equal(Player.INITIAL_MEEPLE_COUNT - 1, playerOne.Meeples);
            Assert.Equal(Player.INITIAL_MEEPLE_COUNT - 1, playerTwo.Meeples);
            Assert.Equal(1, playerOneCity._claimants[playerOne]);
            Assert.Equal(1, playerTwoCity._claimants[playerTwo]);
            Assert.Equal(1, playerOneCity.Score);
            Assert.Equal(1, playerTwoCity.Score);

            claimableLandmarks = session.PlaceTile(TileLayouts.TripleCityShield.Rotate(2), 30, 31);
            var combinedCity = claimableLandmarks.Cities[0];
            session.EndTurn();

            Assert.Equal(1, claimableLandmarks.Cities[0]._claimants[playerOne]);
            Assert.Equal(1, claimableLandmarks.Cities[0]._claimants[playerTwo]);
            Assert.Contains(playerOne, claimableLandmarks.Cities[0].Winners());
            Assert.Contains(playerTwo, claimableLandmarks.Cities[0].Winners());
            Assert.Equal(0, playerOne.Score);
            Assert.Equal(0, playerTwo.Score);
            Assert.Equal(5, combinedCity.Score);

            session.PlaceTile(TileLayouts.SingleCity.Rotate(1), 28, 31);
            session.EndTurn();

            Assert.Equal(12, playerOne.Score);
            Assert.Equal(12, playerTwo.Score);
        }

        // Test merging fields
        [Fact]
        public void Test03()
        {
            var session = Session.New(2);
            var playerOne = session._players[0];
            var playerTwo = session._players[1];

            var claimableLandmarks = session.PlaceTile(TileLayouts.CityBridge.Rotate(1), 30, 31);
            var fieldOne = claimableLandmarks.Fields[0];
            fieldOne.Claim(playerOne);
            session.EndTurn();

            session.PlaceTile(TileLayouts.DoubleCityRoad, 31, 30);
            session.EndTurn();

            session.PlaceTile(TileLayouts.FourCity, 32, 30);
            session.EndTurn();

            session.PlaceTile(TileLayouts.StraightRoadCity.Rotate(2), 29, 30);
            session.EndTurn();

            claimableLandmarks = session.PlaceTile(TileLayouts.DoubleCity, 29, 29);
            var fieldTwo = claimableLandmarks.Fields[0];
            fieldTwo.Claim(playerTwo);
            session.EndTurn();

            session.PlaceTile(TileLayouts.SingleCity.Rotate(3), 30, 29);
            session.EndTurn();

            session.PlaceTile(TileLayouts.SingleCity.Rotate(2), 30, 32);
            session.EndTurn();

            session.PlaceTile(TileLayouts.StraightRoad.Rotate(1), 28, 29);
            session.EndTurn();

            session.PlaceTile(TileLayouts.CurvedRoad.Rotate(1), 28, 30);
            session.EndTurn();

            session.PlaceTile(TileLayouts.StraightRoad.Rotate(1), 28, 29);
            session.EndTurn();

            session.PlaceTile(TileLayouts.CurvedRoad.Rotate(3), 29, 31);
            session.EndTurn();

            session.PlaceTile(TileLayouts.StraightRoad.Rotate(1), 29, 32);
            session.EndTurn();

            Assert.Equal(3, fieldOne.Score);
            Assert.Single(fieldOne.Winners());
            Assert.Contains(playerOne, fieldOne.Winners());

            Assert.Equal(3, fieldTwo.Score);
            Assert.Single(fieldTwo.Winners());
            Assert.Contains(playerTwo, fieldTwo.Winners());

            claimableLandmarks = session.PlaceTile(TileLayouts.ChapelRoad, 31, 29);
            var combinedField = claimableLandmarks.Fields[0];
            session.EndTurn();

            Assert.Equal(6, combinedField.Score);
            Assert.Contains(playerOne, combinedField.Winners());
            Assert.Contains(playerTwo, combinedField.Winners());

            session.Finish();
            Assert.Equal(6, playerOne.Score);
            Assert.Equal(6, playerTwo.Score);
        }

        // Test stealing cities
        [Fact]
        public void Test04()
        {
            var session = Session.New(2);
            var playerOne = session._players[0];
            var playerTwo = session._players[1];
            
            var claimableLandmarks = session.PlaceTile(TileLayouts.TSectionCity, 31, 30);
            var playerOneCity = claimableLandmarks.Cities[0];
            playerOneCity.Claim(playerOne);
            session.EndTurn();

            Assert.Equal(1, playerOneCity.UnclosedEdges);
            Assert.Equal(Player.INITIAL_MEEPLE_COUNT - 1, playerOne.Meeples);

            claimableLandmarks = session.PlaceTile(TileLayouts.StraightRoadCity, 29, 30);
            var playerTwoCity = claimableLandmarks.Cities[0];
            playerTwoCity.Claim(playerTwo);
            session.EndTurn();

            Assert.Equal(Player.INITIAL_MEEPLE_COUNT - 1, playerTwo.Meeples);
            Assert.Equal(1, playerOneCity.UnclosedEdges);
            Assert.Equal(1, playerTwoCity.UnclosedEdges);

            session.PlaceTile(TileLayouts.TripleCity.Rotate(1), 29, 31);
            session.EndTurn();

            Assert.Equal(1, playerOneCity.UnclosedEdges);
            Assert.Equal(2, playerTwoCity.UnclosedEdges);

            session.PlaceTile(TileLayouts.DoubleCity.Rotate(2), 31, 31);
            session.EndTurn();

            Assert.Equal(1, playerOneCity.UnclosedEdges);
            Assert.Equal(2, playerTwoCity.UnclosedEdges);

            claimableLandmarks = session.PlaceTile(TileLayouts.FourCity, 30, 31);
            var mergedCity = claimableLandmarks.Cities[0];
            session.EndTurn();

            Assert.Equal(2, mergedCity.UnclosedEdges);
            Assert.Equal(Player.INITIAL_MEEPLE_COUNT - 1, playerOne.Meeples);
            Assert.Equal(Player.INITIAL_MEEPLE_COUNT - 1, playerTwo.Meeples);

            claimableLandmarks = session.PlaceTile(TileLayouts.AdjacentWalls.Rotate(1), 29, 32);
            if (claimableLandmarks.Cities[0].IsClaimable())
            {
                claimableLandmarks.Cities[0].Claim(playerOne);
            }
            else
            {
                claimableLandmarks.Cities[1].Claim(playerOne);
            }

            Assert.Equal(1, mergedCity.UnclosedEdges);
            Assert.Equal(Player.INITIAL_MEEPLE_COUNT - 2, playerOne.Meeples);
            Assert.Equal(Player.INITIAL_MEEPLE_COUNT - 1, playerTwo.Meeples);

            session.EndTurn();

            claimableLandmarks = session.PlaceTile(TileLayouts.DoubleCity.Rotate(2), 30, 32);
            var finalCity = claimableLandmarks.Cities[0];
            session.EndTurn();

            Assert.Equal(Player.INITIAL_MEEPLE_COUNT, playerOne.Meeples);
            Assert.Equal(Player.INITIAL_MEEPLE_COUNT, playerTwo.Meeples);
            Assert.Equal(0, playerTwo.Score);
            Assert.Equal(18, playerOne.Score);
        }

        // Test chapels
        [Fact]
        public void Test05()
        {
            var session = Session.New(1);
            var playerOne = session._players[0];

            var claimableLandmarks = session.PlaceTile(TileLayouts.Chapel, 30, 29);
            var chapelOne = claimableLandmarks.Chapel;
            chapelOne.Claim(playerOne);
            session.EndTurn();

            Assert.Equal(2, chapelOne.Score);

            session.PlaceTile(TileLayouts.StraightRoad, 31, 30);
            session.EndTurn();

            Assert.Equal(3, chapelOne.Score);

            session.PlaceTile(TileLayouts.CurvedRoad.Rotate(3), 32, 30);
            session.EndTurn();

            Assert.Equal(3, chapelOne.Score);

            session.PlaceTile(TileLayouts.OppositeWalls, 30, 28);
            session.EndTurn();

            Assert.Equal(4, chapelOne.Score);

            claimableLandmarks = session.PlaceTile(TileLayouts.Chapel, 31, 29);
            var chapelTwo = claimableLandmarks.Chapel;
            chapelTwo.Claim(playerOne);
            session.EndTurn();

            Assert.Equal(5, chapelOne.Score);
            Assert.Equal(6, chapelTwo.Score);

            claimableLandmarks = session.PlaceTile(TileLayouts.ChapelRoad, 32, 31);
            var chapelThree = claimableLandmarks.Chapel;
            chapelThree.Claim(playerOne);
            session.EndTurn();

            Assert.Equal(5, chapelOne.Score);
            Assert.Equal(6, chapelTwo.Score);
            Assert.Equal(3, chapelThree.Score);

            session.PlaceTile(TileLayouts.SingleCity.Rotate(3), 31, 28);
            session.EndTurn();

            Assert.Equal(6, chapelOne.Score);
            Assert.Equal(7, chapelTwo.Score);
            Assert.Equal(3, chapelThree.Score);

            session.PlaceTile(TileLayouts.CityBridge.Rotate(1), 32, 28);
            session.EndTurn();

            Assert.Equal(6, chapelOne.Score);
            Assert.Equal(8, chapelTwo.Score);
            Assert.Equal(3, chapelThree.Score);

            session.PlaceTile(TileLayouts.SingleCity.Rotate(2), 32, 29);
            session.EndTurn();

            Assert.Equal(6, chapelOne.Score);
            Assert.Equal(9, chapelTwo.Score);
            Assert.Equal(3, chapelThree.Score);
            Assert.Equal(9, playerOne.Score);

            session.Finish();

            Assert.Equal(18, playerOne.Score);
        }
    }
}
*/