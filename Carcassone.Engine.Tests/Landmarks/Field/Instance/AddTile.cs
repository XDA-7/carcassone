using Carcassone.Engine;
using Carcassone.Engine.Tests;
using Xunit;

namespace Carcassone.Engine.Tests_Field
{
    public class AddTile
    {
        [Fact]
        public void AddsCorners()
        {
            var field = new Field();
            var edgedCorners = CreateEdgedCorners(new TileEdge[]
            {
                new TileEdge(EdgeType.Blank),
                new TileEdge(EdgeType.Blank),
                new TileEdge(EdgeType.Blank),
                new TileEdge(EdgeType.Blank)
            });

            field.AddTile(edgedCorners);

            var addedCorners = field.TileCorners.ToArray();
            for (var i = 0; i < edgedCorners.Length; i++)
            {
                Assert.Equal(edgedCorners[i].TileCorner, addedCorners[i]);
            }
        }

        [Fact]
        public void AddsCities()
        {
            var field = new Field();
            var tileEdges = new TileEdge[]
            {
                new TileEdge(EdgeType.City),
                new TileEdge(EdgeType.Blank),
                new TileEdge(EdgeType.City),
                new TileEdge(EdgeType.Blank)
            };
            var city1 = new City();
            var city2 = new City();
            tileEdges[0].Landmark = city1;
            tileEdges[2].Landmark = city2;
            var edgedCorners = CreateEdgedCorners(tileEdges);

            field.AddTile(edgedCorners);

            var cities = field.Cities;
            Assert.Equal(2, cities.Length);
            Assert.Equal(city1, cities[0]);
            Assert.Equal(city2, cities[1]);
        }

        [Fact]
        public void AddsDuplicateCitiesOnce()
        {
            var field = new Field();
            var tileEdges = new TileEdge[]
            {
                new TileEdge(EdgeType.City),
                new TileEdge(EdgeType.Blank),
                new TileEdge(EdgeType.City),
                new TileEdge(EdgeType.Blank)
            };
            var city = new City();
            tileEdges[0].Landmark = city;
            tileEdges[2].Landmark = city;
            var edgedCorners = CreateEdgedCorners(tileEdges);

            field.AddTile(edgedCorners);

            var cities = field.Cities;
            Assert.Equal(1, cities.Length);
            Assert.Equal(city, cities[0]);
        }

        private EdgedCorner[] CreateEdgedCorners(TileEdge[] tileEdges)
        {
            return new EdgedCorner[]
            {
                new EdgedCorner(new TileCorner(), tileEdges[0], tileEdges[1]),
                new EdgedCorner(new TileCorner(), tileEdges[1], tileEdges[2]),
                new EdgedCorner(new TileCorner(), tileEdges[2], tileEdges[3]),
                new EdgedCorner(new TileCorner(), tileEdges[3], tileEdges[0])
            };
        }
    }
}