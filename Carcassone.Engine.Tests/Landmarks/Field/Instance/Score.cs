using System.Collections.Generic;
using System.Reflection;
using Carcassone.Engine;
using Carcassone.Engine.Tests;
using Xunit;

namespace Carcassone.Engine.Tests_Field
{
    public class Score
    {
        [Fact]
        public void ScoresCompletedFields()
        {
            var field = new Field();
            var mockCities = new HashSet<City>
            {
                new MockCity { IsCompleteHook = true },
                new MockCity { IsCompleteHook = true },
                new MockCity { IsCompleteHook = true },
                new MockCity { IsCompleteHook = false },
                new MockCity { IsCompleteHook = false },
                new MockCity { IsCompleteHook = false }
            };
            
            field.SetPrivateField("_cities", mockCities);

            Assert.Equal(9, field.Score);
        }
    }
}