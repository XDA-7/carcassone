using System.Collections.Generic;
using Carcassone.Engine;
using Carcassone.Engine.Tests;
using Xunit;

namespace Carcassone.Engine.Tests_Field
{
    public class Merge
    {
        [Fact]
        public void MergedFieldContainsAllCities()
        {
            var leftCities = new HashSet<City>
            {
                new City(),
                new City(),
                new City()
            };

            var rightCities = new HashSet<City>
            {
                new City(),
                new City(),
                new City()
            };

            var mergedField = CreateMergedField(
                leftCities,
                new List<TileCorner>(),
                rightCities,
                new List<TileCorner>());
            var mergedCities = mergedField.Cities;
            Assert.Equal(6, mergedCities.Length);
            Assert.Contains(leftCities.ToArray()[0], mergedCities);
            Assert.Contains(leftCities.ToArray()[1], mergedCities);
            Assert.Contains(leftCities.ToArray()[2], mergedCities);
            Assert.Contains(rightCities.ToArray()[0], mergedCities);
            Assert.Contains(rightCities.ToArray()[1], mergedCities);
            Assert.Contains(rightCities.ToArray()[2], mergedCities);
        }

        [Fact]
        public void MergedFieldContainsAllCorners()
        {
            var leftCorners = new List<TileCorner>
            {
                new TileCorner(),
                new TileCorner(),
                new TileCorner()
            };

            var rightCorners = new List<TileCorner>
            {
                new TileCorner(),
                new TileCorner(),
                new TileCorner()
            };

            var mergedField = CreateMergedField(
                new HashSet<City>(),
                leftCorners,
                new HashSet<City>(),
                rightCorners);
            var mergedTileCorners = mergedField.TileCorners.ToArray();
            Assert.Equal(6, mergedTileCorners.Length);
            Assert.Contains(leftCorners[0], mergedTileCorners);
            Assert.Contains(leftCorners[1], mergedTileCorners);
            Assert.Contains(leftCorners[2], mergedTileCorners);
            Assert.Contains(rightCorners[0], mergedTileCorners);
            Assert.Contains(rightCorners[1], mergedTileCorners);
            Assert.Contains(rightCorners[2], mergedTileCorners);
        }

        [Fact]
        public void MergedFieldDoesNotContainDuplicateCities()
        {
            var duplicateCity = new City();
            var leftCities = new HashSet<City>
            {
                new City(),
                new City(),
                duplicateCity
            };

            var rightCities = new HashSet<City>
            {
                new City(),
                new City(),
                duplicateCity
            };
            var mergedField = CreateMergedField(
                leftCities,
                new List<TileCorner>(),
                rightCities,
                new List<TileCorner>());
            var mergedCities = mergedField.Cities;
            Assert.Equal(5, mergedCities.Length);
            Assert.Contains(leftCities.ToArray()[0], mergedCities);
            Assert.Contains(leftCities.ToArray()[1], mergedCities);
            Assert.Contains(leftCities.ToArray()[2], mergedCities);
            Assert.Contains(rightCities.ToArray()[0], mergedCities);
            Assert.Contains(rightCities.ToArray()[1], mergedCities);
            Assert.Contains(rightCities.ToArray()[2], mergedCities);
        }

        private Field CreateMergedField(
            HashSet<City> leftCities,
            List<TileCorner> leftCorners,
            HashSet<City> rightCities,
            List<TileCorner> rightCorners)
        {
            var leftField = new Field();
            leftField.SetPrivateField("_cities", leftCities);
            leftField.SetPrivateField("_tileCorners", leftCorners);
            var rightField = new Field();
            rightField.SetPrivateField("_cities", rightCities);
            rightField.SetPrivateField("_tileCorners", rightCorners);
            return Field.Merge(leftField, rightField);
        }
    }
}