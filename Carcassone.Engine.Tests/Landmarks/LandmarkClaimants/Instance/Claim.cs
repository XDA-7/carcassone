using System.Collections.Generic;
using Carcassone.Engine;
using Xunit;

namespace Carcassone.Engine.Tests_LandmarkClaimants
{
    public class Claim
    {
        [Fact]
        public void AddsOneMeepleToPlayerClaims()
        {
            var players = new Player[]
            {
                new Player { Meeples = 2 },
                new Player { Meeples = 1 }
            };

            var landmarkClaimants = new LandmarkClaimants();
            landmarkClaimants.Claim(players[0]);
            landmarkClaimants.Claim(players[1]);
            var result = landmarkClaimants.GetClaimants();

            Assert.Contains(new KeyValuePair<Player, int>(players[0], 1), result);
            Assert.Contains(new KeyValuePair<Player, int>(players[1], 1), result);
        }

        [Fact]
        public void ReducesPlayerMeepleCountByOne()
        {
            var players = new Player[]
            {
                new Player { Meeples = 2 },
                new Player { Meeples = 1 }
            };

            var landmarkClaimants = new LandmarkClaimants();
            landmarkClaimants.Claim(players[0]);
            landmarkClaimants.Claim(players[1]);

            Assert.Equal(1, players[0].Meeples);
            Assert.Equal(0, players[1].Meeples);
        }
    }
}