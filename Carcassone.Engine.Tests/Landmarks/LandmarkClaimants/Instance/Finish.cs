using System.Collections.Generic;
using Carcassone.Engine;
using Carcassone.Engine.Tests;
using Xunit;

namespace Carcassone.Engine.Tests_LandmarkClaimants
{
    public class Finish
    {
        [Fact]
        public void ReturnsMeeplesToPlayers()
        {
            var players = new Player[]
            {
                new Player { Meeples = 0 },
                new Player { Meeples = 0 },
                new Player { Meeples = 0 }
            };

            var meepleCounts = new Dictionary<Player, int>
            {
                { players[0], 1 },
                { players[1], 2 },
                { players[2], 1 }
            };

            var landmarkClaimants = new LandmarkClaimants();
            landmarkClaimants.SetPrivateField("_meepleCounts", meepleCounts);

            landmarkClaimants.Finish();

            Assert.Equal(1, players[0].Meeples);
            Assert.Equal(2, players[1].Meeples);
            Assert.Equal(1, players[2].Meeples);
        }
    }
}