using System.Collections.Generic;
using Carcassone.Engine;
using Carcassone.Engine.Tests;
using Xunit;

namespace Carcassone.Engine.Tests_LandmarkClaimants
{
    public class Winners
    {
        [Fact]
        public void ReturnsAllPlayersWithEqualMostClaims()
        {
            var players = new Player[]
            {
                new Player(),
                new Player(),
                new Player(),
                new Player()
            };

            var meepleCounts = new Dictionary<Player, int>
            {
                { players[0], 1 },
                { players[1], 3 },
                { players[2], 2 },
                { players[3], 3 }
            };

            var landmarkClaimants = new LandmarkClaimants();
            landmarkClaimants.SetPrivateField("_meepleCounts", meepleCounts);

            var result = landmarkClaimants.Winners().ToArray();

            Assert.Equal(2, result.Length);
            Assert.Contains(players[1], result);
            Assert.Contains(players[3], result);
        }
    }
}