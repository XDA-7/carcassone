using System.Collections.Generic;
using Carcassone.Engine;
using Carcassone.Engine.Tests;
using Xunit;

namespace Carcassone.Engine.Tests_LandmarkClaimants
{
    public class Clone
    {
        [Fact]
        public void SetsClaimantsFromParameter()
        {
            var players = new Player[]
            {
                new Player(),
                new Player()
            };

            var claimants = new PlayerClaim[]
            {
                new PlayerClaim(players[0], 2),
                new PlayerClaim(players[1], 1)
            };

            var clone = LandmarkClaimants.Clone(claimants);
            var result = clone.GetClaimants().ToArray();

            Assert.Equal(2, result.Length);
            Assert.Contains(new KeyValuePair<Player, int>(players[0], 2), result);
            Assert.Contains(new KeyValuePair<Player, int>(players[1], 1), result);
        }
    }
}