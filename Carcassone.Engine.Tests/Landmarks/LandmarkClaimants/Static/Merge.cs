using System.Collections.Generic;
using Carcassone.Engine;
using Carcassone.Engine.Tests;
using Xunit;

namespace Carcassone.Engine.Tests_LandmarkClaimants
{
    public class Merge
    {
        [Fact]
        public void CombinesPlayerClaims()
        {
            var player = new Player[]
            {
                new Player(),
                new Player(),
                new Player(),
                new Player()
            };

            var leftLandmarkClaimants = new LandmarkClaimants();
            leftLandmarkClaimants.SetPrivateField("_meepleCounts", new Dictionary<Player, int>
            {
                { player[0], 1 },
                { player[1], 1 },
                { player[2], 1 }
            });

            var rightLandmarkClaimants = new LandmarkClaimants();
            rightLandmarkClaimants.SetPrivateField("_meepleCounts", new Dictionary<Player, int>
            {
                { player[2], 2 },
                { player[3], 1 }
            });

            var merged = LandmarkClaimants.Merge(leftLandmarkClaimants, rightLandmarkClaimants);
            var claimants = merged.GetClaimants().ToArray();

            Assert.Equal(4, claimants.Length);
            Assert.Contains(new KeyValuePair<Player, int>(player[0], 1), claimants);
            Assert.Contains(new KeyValuePair<Player, int>(player[1], 1), claimants);
            Assert.Contains(new KeyValuePair<Player, int>(player[2], 3), claimants);
            Assert.Contains(new KeyValuePair<Player, int>(player[3], 1), claimants);
        }
    }
}