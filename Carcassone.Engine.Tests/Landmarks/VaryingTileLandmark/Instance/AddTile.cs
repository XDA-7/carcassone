using Carcassone.Engine;
using Carcassone.Engine.Tests;
using Xunit;

namespace Carcassone.Engine.Tests_VaryingTileLandmark
{
    public class AddTile
    {
        [Fact]
        public void AddsTileToCollection()
        {
            var landmark = new MockVaryingTileLandmark();
            var tile = Tile.Blank();

            landmark.AddTile(tile, new TileEdgePair[0]);
            var tiles = landmark.GetTiles().ToArray();

            Assert.Equal(1, tiles.Length);
            Assert.Equal(tile, tiles[0]);
        }

        [Fact]
        public void AssignsLandmarkTileEdges()
        {
            var result = GetAddedTileEdgePairs(out var landmark);
            Assert.Equal(landmark, result[0].Edge.Landmark);
            Assert.Equal(landmark, result[1].Edge.Landmark);
            Assert.Equal(landmark, result[2].Edge.Landmark);
        }

        [Fact]
        public void DoesNotAssignLandmarkToAdjacentEdges()
        {
            var result = GetAddedTileEdgePairs(out var landmark);
            Assert.Null(result[0].AdjacentEdge.Landmark);
            Assert.Null(result[1].AdjacentEdge.Landmark);
            Assert.Null(result[2].AdjacentEdge.Landmark);
        }

        private TileEdgePair[] GetAddedTileEdgePairs(out VaryingTileLandmark landmark)
        {
            landmark = new MockVaryingTileLandmark();
            var tile = new Tile(TileLayouts.TripleCity);
            var edgePairs = new TileEdgePair[]
            {
                new TileEdgePair(tile.GetEdge(0), new TileEdge(EdgeType.City)),
                new TileEdgePair(tile.GetEdge(1), new TileEdge(EdgeType.City)),
                new TileEdgePair(tile.GetEdge(3), new TileEdge(EdgeType.City))
            };

            landmark.AddTile(tile, edgePairs);
            return edgePairs;
        }

        [Fact]
        public void IncreasesUnclosedEdgeCountForUnmatchedEdges()
        {
            var landmark = new MockVaryingTileLandmark();
            var edgePairs = new TileEdgePair[]
            {
                new TileEdgePair(new TileEdge(EdgeType.City), new TileEdge(EdgeType.Blank)),
                new TileEdgePair(new TileEdge(EdgeType.City), new TileEdge(EdgeType.Blank))
            };

            landmark.AddTile(Tile.Blank(), edgePairs);

            Assert.Equal(2, landmark.UnclosedEdges);
        }

        [Fact]
        public void DecreasesUnclosedEdgeCountForMatchedEdges()
        {
            var landmark = new MockVaryingTileLandmark();
            var firstEdge = new TileEdge(EdgeType.City);
            var edgePairs = new TileEdgePair[]
            {
                new TileEdgePair(firstEdge, new TileEdge(EdgeType.Blank))
            };

            landmark.AddTile(Tile.Blank(), edgePairs);

            edgePairs = new TileEdgePair[]
            {
                new TileEdgePair(new TileEdge(EdgeType.City), firstEdge)
            };

            landmark.AddTile(Tile.Blank(), edgePairs);

            Assert.Equal(0, landmark.UnclosedEdges);
        }

        [Fact]
        public void ModifiesUnclosedEdgeCountForMatchedAndUnmatchedEdges()
        {
            var landmark = new MockVaryingTileLandmark();
            var firstEdge = new TileEdge(EdgeType.City);
            var edgePairs = new TileEdgePair[]
            {
                new TileEdgePair(firstEdge, new TileEdge(EdgeType.Blank)),
                new TileEdgePair(new TileEdge(EdgeType.City), new TileEdge(EdgeType.Blank))
            };

            landmark.AddTile(Tile.Blank(), edgePairs);

            edgePairs = new TileEdgePair[]
            {
                new TileEdgePair(new TileEdge(EdgeType.City), firstEdge)
            };

            landmark.AddTile(Tile.Blank(), edgePairs);

            Assert.Equal(1, landmark.UnclosedEdges);
        }
    }
}