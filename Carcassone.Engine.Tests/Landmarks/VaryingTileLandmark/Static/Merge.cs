using System;
using System.Collections.Generic;
using Carcassone.Engine;
using Carcassone.Engine.Tests;
using Xunit;

namespace Carcassone.Engine.Tests_VaryingTileLandmark
{
    public class Merge
    {
        [Fact]
        public void ReturnsLandmarkWithTilesFromBoth()
        {
            var destinationTiles = GetFiveBlankTiles();
            var sourceTiles = GetFiveBlankTiles();
            var destination = MockLandmarksWithTiles(destinationTiles);
            var source = MockLandmarksWithTiles(sourceTiles);

            var result = VaryingTileLandmark.Merge(destination, source);
            var resultTiles = result.GetTiles();

            for (var i = 0; i < 5; i++)
            {
                Assert.Contains(destinationTiles[i], resultTiles);
                Assert.Contains(sourceTiles[i], resultTiles);
            }
        }

        [Fact]
        public void TileEdgesReferenceMergedLandmark()
        {
            var destinationTiles = GetFiveBlankTiles();
            var sourceTiles = GetFiveBlankTiles();
            var destination = MockLandmarksWithTiles(destinationTiles);
            var source = MockLandmarksWithTiles(sourceTiles);

            AssignLandmarkToTileEdges(destination, destinationTiles,
            (0, 1), (0, 2), (1, 0), (2, 3), (3, 1), (4, 0), (4, 2));
            AssignLandmarkToTileEdges(source, sourceTiles,
            (0, 0), (0, 2), (1, 2), (2, 3), (3, 2), (4, 1), (4, 2));

            var result = VaryingTileLandmark.Merge(destination, source);
            
            Assert.Equal(result, destinationTiles[0].GetEdge(1).Landmark);
            Assert.Equal(result, destinationTiles[0].GetEdge(2).Landmark);
            Assert.Equal(result, destinationTiles[1].GetEdge(0).Landmark);
            Assert.Equal(result, destinationTiles[2].GetEdge(3).Landmark);
            Assert.Equal(result, destinationTiles[3].GetEdge(1).Landmark);
            Assert.Equal(result, destinationTiles[4].GetEdge(0).Landmark);
            Assert.Equal(result, destinationTiles[4].GetEdge(2).Landmark);

            Assert.Equal(result, sourceTiles[0].GetEdge(0).Landmark);
            Assert.Equal(result, sourceTiles[0].GetEdge(2).Landmark);
            Assert.Equal(result, sourceTiles[1].GetEdge(2).Landmark);
            Assert.Equal(result, sourceTiles[2].GetEdge(3).Landmark);
            Assert.Equal(result, sourceTiles[3].GetEdge(2).Landmark);
            Assert.Equal(result, sourceTiles[4].GetEdge(1).Landmark);
            Assert.Equal(result, sourceTiles[4].GetEdge(2).Landmark);
        }

        [Fact]
        public void MergedLandmarksCombineScores()
        {
            var destinationTiles = GetFiveBlankTiles();
            var sourceTiles = GetFiveBlankTiles();
            var destination = MockLandmarksWithTiles(destinationTiles);
            var source = MockLandmarksWithTiles(sourceTiles);

            Assert.Equal(5, destination.Score);
            Assert.Equal(5, source.Score);

            var result = VaryingTileLandmark.Merge(destination, source);

            Assert.Equal(10, result.Score);
        }

        [Fact]
        public void MergedLandmarksWithSharedTilesAreCorrectlyScored()
        {
            var sharedTiles = new Tile[] { Tile.Blank(), Tile.Blank() };
            var destinationTiles = GetFiveBlankTiles();
            var sourceTiles = GetFiveBlankTiles();
            destinationTiles[0] = sharedTiles[0];
            sourceTiles[0] = sharedTiles[0];
            destinationTiles[1] = sharedTiles[1];
            sourceTiles[1] = sharedTiles[1];
            var destination = MockLandmarksWithTiles(destinationTiles);
            var source = MockLandmarksWithTiles(sourceTiles);

            Assert.Equal(5, destination.Score);
            Assert.Equal(5, source.Score);

            var result = VaryingTileLandmark.Merge(destination, source);

            Assert.Equal(8, result.Score);
        }

        [Fact]
        public void UnclosedEdgesIsSumOfMergedLandmarks()
        {
            var destination = new MockVaryingTileLandmark();
            destination.SetPrivateField("_unclosedEdges", 3);
            var source = new MockVaryingTileLandmark();
            source.SetPrivateField("_unclosedEdges", 5);

            var result = VaryingTileLandmark.Merge(destination, source);
            Assert.Equal(8, result.UnclosedEdges);
        }

        private Tile[] GetFiveBlankTiles()
        {
            return new Tile[]
            {
                Tile.Blank(),
                Tile.Blank(),
                Tile.Blank(),
                Tile.Blank(),
                Tile.Blank()
            };
        }

        private VaryingTileLandmark MockLandmarksWithTiles(IEnumerable<Tile> tiles)
        {
            var landmark = new MockVaryingTileLandmark();
            foreach (var tile in tiles)
            {
                landmark.AddTile(tile, new TileEdgePair[0]);
            }

            return landmark;
        }

        private void AssignLandmarkToTileEdges(VaryingTileLandmark landmark, Tile[] tiles, params (int, int)[] tileEdges)
        {
            foreach (var tileEdge in tileEdges)
            {
                tiles[tileEdge.Item1].GetEdge(tileEdge.Item2).Landmark = landmark;
            }
        }
    }
}