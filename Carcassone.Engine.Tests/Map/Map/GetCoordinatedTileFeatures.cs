using System.Collections.Generic;
using Carcassone.Engine;
using Carcassone.Engine.Tests;
using Xunit;

namespace Carcassone.Engine.Tests_Map
{
    public class GetCoordinatedTileFeatures
    {
        [Fact]
        public void GetsCoordinatedFeaturesOfAllSetTiles()
        {
            var tiles = new Tile[]
            {
                Tile.Blank(),
                Tile.Blank(),
                Tile.Blank(),
                Tile.Blank(),
                Tile.Blank()
            };

            var tileMatrix = new TileMatrix(30);
            tileMatrix[7, 12] = tiles[0];
            tileMatrix[7, 23] = tiles[1];
            tileMatrix[22, 7] = tiles[2];
            tileMatrix[14, 13] = tiles[3];
            tileMatrix[19, 28] = tiles[4];
            var map = new MockMap(30);
            map.SetPrivateField("_tileMatrix", tileMatrix);

            var result = map.GetCoordinatedTileFeatures();

            AssertContainsCornersOfTile(result.TileCorners, tiles[0], 7, 12);
            AssertContainsEdgesOfTile(result.TileEdges, tiles[0], 7, 12);

            AssertContainsCornersOfTile(result.TileCorners, tiles[1], 7, 23);
            AssertContainsEdgesOfTile(result.TileEdges, tiles[1], 7, 23);

            AssertContainsCornersOfTile(result.TileCorners, tiles[2], 22, 7);
            AssertContainsEdgesOfTile(result.TileEdges, tiles[2], 22, 7);

            AssertContainsCornersOfTile(result.TileCorners, tiles[3], 14, 13);
            AssertContainsEdgesOfTile(result.TileEdges, tiles[3], 14, 13);

            AssertContainsCornersOfTile(result.TileCorners, tiles[4], 19, 28);
            AssertContainsEdgesOfTile(result.TileEdges, tiles[4], 19, 28);
        }

        private void AssertContainsCornersOfTile(
            IEnumerable<CoordinatedTileCorner> coordinatedTileCorners,
            Tile tile,
            int x,
            int y)
        {
            AssertContainsTileCorner(coordinatedTileCorners, tile, x, y, 0);
            AssertContainsTileCorner(coordinatedTileCorners, tile, x, y, 1);
            AssertContainsTileCorner(coordinatedTileCorners, tile, x, y, 2);
            AssertContainsTileCorner(coordinatedTileCorners, tile, x, y, 3);
        }

        private void AssertContainsTileCorner(
            IEnumerable<CoordinatedTileCorner> coordinatedTileCorners,
            Tile tile,
            int x,
            int y,
            int edge)
        {
            Assert.Contains(coordinatedTileCorners, coordinatedTileCorner =>
                coordinatedTileCorner.EdgeCoordinate.Str() == $"({x},{y},{edge})" &&
                coordinatedTileCorner.TileCorner == tile.GetCorner(edge));
        }

        private void AssertContainsEdgesOfTile(
            IEnumerable<CoordinatedTileEdge> coordinatedTileEdges,
            Tile tile,
            int x,
            int y)
        {
            AssertContainsTileEdge(coordinatedTileEdges, tile, x, y, 0);
            AssertContainsTileEdge(coordinatedTileEdges, tile, x, y, 1);
            AssertContainsTileEdge(coordinatedTileEdges, tile, x, y, 2);
            AssertContainsTileEdge(coordinatedTileEdges, tile, x, y, 3);
        }

        private void AssertContainsTileEdge(
            IEnumerable<CoordinatedTileEdge> coordinatedTileEdges,
            Tile tile,
            int x,
            int y,
            int edge)
        {
            Assert.Contains(coordinatedTileEdges, coordinatedTileEdge =>
                coordinatedTileEdge.EdgeCoordinate.Str() == $"({x},{y},{edge})" &&
                coordinatedTileEdge.TileEdge == tile.GetEdge(edge));
        }
    }
}