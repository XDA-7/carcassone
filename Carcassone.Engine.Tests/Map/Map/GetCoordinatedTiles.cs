using Carcassone.Engine;
using Carcassone.Engine.Tests;
using Xunit;

namespace Carcassone.Engine.Tests_Map
{
    public class GetCoordinatedTiles
    {
        [Fact]
        public void ReturnsTilesSetInMap()
        {
            var map = new MockMap(30);
            map.SetTile(TileLayouts.ChapelRoad, 12, 10);
            map.SetTile(TileLayouts.DoubleCityRoadShield, 15, 21);
            map.SetTile(TileLayouts.CurvedRoad, 21, 14);
            map.SetTile(TileLayouts.AllRoad, 0, 9);
            map.SetTile(TileLayouts.QuestionMarkRight, 17, 22);

            var result = map.GetCoordinatedTiles();

            Assert.Contains(
                result,
                x => x.Coordinate.Str() == "(12,10)" &&
                x.Tile.Str() == TileLayouts.ChapelRoad.Str());
            Assert.Contains(
                result,
                x => x.Coordinate.Str() == "(15,21)" &&
                x.Tile.Str() == TileLayouts.DoubleCityRoadShield.Str());
            Assert.Contains(
                result,
                x => x.Coordinate.Str() == "(21,14)" &&
                x.Tile.Str() == TileLayouts.CurvedRoad.Str());
            Assert.Contains(
                result,
                x => x.Coordinate.Str() == "(0,9)" &&
                x.Tile.Str() == TileLayouts.AllRoad.Str());
            Assert.Contains(
                result,
                x => x.Coordinate.Str() == "(17,22)" &&
                x.Tile.Str() == TileLayouts.QuestionMarkRight.Str());
        }
    }
}