using System.Collections.Generic;
using Carcassone.Engine;
using Carcassone.Engine.Tests;
using Xunit;

namespace Carcassone.Engine.Tests_Map
{
    public class GetPlaceableCoordinates
    {
        [Fact]
        public void ReturnsCoordinatesNotBlockedByAdjacentCorners()
        {
            var map = SetupMap();
            var result = map.GetPlaceableCoordinates(TileLayouts.Chapel);
            Assert.Contains(result, coordinate => coordinate.Str() == "(2,20)");
            Assert.Contains(result, coordinate => coordinate.Str() == "(12,13)");
            Assert.Contains(result, coordinate => coordinate.Str() == "(9,3)");
        }

        [Fact]
        public void ReturnsNoCoordinatesIfBlockedByAdjacentCorner()
        {
            var map = SetupMap();
            var result = map.GetPlaceableCoordinates(TileLayouts.SingleCity);
            Assert.Empty(result);
        }

        private Map SetupMap()
        {
            var map = new MockMap(30);
            map.GetAdjacentEdgesValue = new TileEdge[]
            {
                new TileEdge(EdgeType.Field),
                new TileEdge(EdgeType.Field),
                new TileEdge(EdgeType.Field),
                new TileEdge(EdgeType.Field),
            };

            var placeableCoordinates = new PlaceableCoordinates();
            placeableCoordinates.SetPrivateField(
                "_availableCoordinates",
                new HashSet<Coordinate>
                {
                    new Coordinate(2, 20),
                    new Coordinate(12, 13),
                    new Coordinate(9, 3)
                });
            map.SetPrivateField<Map>("_placeableCoordinates", placeableCoordinates);
            return map;
        }
    }
}