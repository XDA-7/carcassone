using System.Collections.Generic;
using Carcassone.Engine;
using Carcassone.Engine.Tests;
using Xunit;

namespace Carcassone.Engine.Tests_Map
{
    public class SetTile
    {
        [Fact]
        public void TheTileSetIsAccessibleFromLocation()
        {
            var map = new MockMap(30);

            map.SetTile(TileLayouts.CityBridgeShield, 23, 12);
            var tile = map.GetTile(23, 12);
            Assert.Equal("fcfccgS", tile.Str());
        }

        [Fact]
        public void PlaceableCoordinatesIsUpdatedWithAdjacentTiles()
        {
            var map = new MockMap(30);
            var coordinates = new Coordinate[]
            {
                new Coordinate(1, 2),
                new Coordinate(1, 3),
                new Coordinate(2, 4),
                new Coordinate(0, 5)
            };

            map.GetAdjacentCoordinatesValue = coordinates;

            map.SetTile(TileLayouts.CityBridgeShield, 23, 12);
            var placeableCoordinates = map
                .GetPrivateField<Map, PlaceableCoordinates>("_placeableCoordinates")
                .Get();
            
            AssertContainsCoordinate(placeableCoordinates, 1, 2);
            AssertContainsCoordinate(placeableCoordinates, 1, 3);
            AssertContainsCoordinate(placeableCoordinates, 2, 4);
            AssertContainsCoordinate(placeableCoordinates, 0, 5);
        }

        private void AssertContainsCoordinate(IEnumerable<Coordinate> coordinates, int x, int y)
        {
            Assert.Contains(coordinates, coordinate => coordinate.X == x && coordinate.Y == y);
        }
    }
}