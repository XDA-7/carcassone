using Carcassone.Engine;
using Xunit;

namespace Carcassone.Engine.Tests_PlaceableCoordinates
{
    public class Update
    {
        [Fact]
        public void SetsExposedCoordinatesAsAvailable()
        {
            var placeableCoordinates = new PlaceableCoordinates();
            var usedCoordinate = new Coordinate(10, 20);
            var exposedCoordinates = new Coordinate[]
            {
                new Coordinate(12, 8),
                new Coordinate(9, 10),
                new Coordinate(20, 15),
                new Coordinate(16, 3)
            };

            placeableCoordinates.Update(usedCoordinate, exposedCoordinates);
            var result = placeableCoordinates.Get();

            Assert.Contains(exposedCoordinates[0], result);
            Assert.Contains(exposedCoordinates[1], result);
            Assert.Contains(exposedCoordinates[2], result);
            Assert.Contains(exposedCoordinates[3], result);
            Assert.DoesNotContain(usedCoordinate, result);
        }

        [Fact]
        public void RemovesUsedCoordinateFromAvailable()
        {
            var placeableCoordinates = new PlaceableCoordinates();
            var exposedCoordinate = new Coordinate[] { new Coordinate(20, 30) };
            placeableCoordinates.Update(new Coordinate(0, 0), exposedCoordinate);
            var usedCoordinate = new Coordinate(20, 30);
            placeableCoordinates.Update(usedCoordinate, new Coordinate[0]);
            var result = placeableCoordinates.Get();

            Assert.Empty(result);
        }

        [Fact]
        public void DoesNotAddExposedCoordinateToAvailableIfPreviouslyUsed()
        {
            var placeableCoordinates = new PlaceableCoordinates();
            placeableCoordinates.Update(new Coordinate(5, 6), new Coordinate[0]);
            placeableCoordinates.Update(new Coordinate(0, 0), new Coordinate[]
            {
                new Coordinate(5, 6)
            });

            var result = placeableCoordinates.Get();
            Assert.Empty(result);
        }

        [Fact]
        public void DoesNotAddExposedIfAlsoPassedAsUsed()
        {
            var placeableCoordinates = new PlaceableCoordinates();
            placeableCoordinates.Update(new Coordinate(12, 21), new Coordinate[]
            {
                new Coordinate(12, 21)
            });

            var result = placeableCoordinates.Get();

            Assert.Empty(result);
        }
    }
}