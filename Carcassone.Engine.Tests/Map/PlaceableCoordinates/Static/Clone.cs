using System.Collections.Generic;
using Carcassone.Engine;
using Carcassone.Engine.Tests;
using Xunit;

namespace Carcassone.Engine.Tests_PlaceableCoordinates
{
    public class Clone
    {
        [Fact]
        public void CloneContainsAllAvailableAndUsedCoordinates()
        {
            var original = new PlaceableCoordinates();
            var availableCoordinates = new HashSet<Coordinate>
            {
                new Coordinate(2, 5),
                new Coordinate(5, 3),
                new Coordinate(17, 5)
            };

            var usedCoordinates = new HashSet<Coordinate>
            {
                new Coordinate(27, 15),
                new Coordinate(21, 11)
            };

            original.SetPrivateField("_availableCoordinates", availableCoordinates);
            original.SetPrivateField("_usedCoordinates", usedCoordinates);
            var clone = PlaceableCoordinates.Clone(original);
            var availableClone = clone
                .GetPrivateField<PlaceableCoordinates, HashSet<Coordinate>>("_availableCoordinates");
            var usedClone = clone
                .GetPrivateField<PlaceableCoordinates, HashSet<Coordinate>>("_usedCoordinates");
            
            Assert.Contains(availableClone, coordinate => coordinate.Str() == "(2,5)");
            Assert.Contains(availableClone, coordinate => coordinate.Str() == "(5,3)");
            Assert.Contains(availableClone, coordinate => coordinate.Str() == "(17,5)");
            Assert.Contains(usedCoordinates, coordinate => coordinate.Str() == "(27,15)");
            Assert.Contains(usedCoordinates, coordinate => coordinate.Str() == "(21,11)");
        }
    }
}