using Carcassone.Engine;
using Carcassone.Engine.Tests;
using Xunit;

namespace Carcassone.Engine.Tests_SurroundingTileLandmarkMap
{
    public class Update
    {
        [Fact]
        public void SetsLandmarkInCenterCoordinate()
        {
            var map = new SurroundingTileLandmarkMap<MockSurroundingTileLandmark>(
                30,
                (count) => new MockSurroundingTileLandmark(count));
            var centerCoordinate = new Coordinate(15, 23);
            var surroundingCoordinates = new Coordinate[]
            {
                new Coordinate(14, 23),
                new Coordinate(15, 24),
                new Coordinate(16, 23),
                new Coordinate(15, 22)
            };

            var result = map.Update(centerCoordinate, surroundingCoordinates);
            var landmarks = map.GetLandmarks();
            var landmark = Assert.Single(landmarks);
            Assert.Equal(landmark, result);
        }

        [Fact]
        public void AddsTileToSurroundingLandmarks()
        {
            var map = new SurroundingTileLandmarkMap<MockSurroundingTileLandmark>(
                30,
                (count) => new MockSurroundingTileLandmark(count));
            var surroundingCoordinates = new Coordinate[]
            {
                new Coordinate(14, 23),
                new Coordinate(15, 24),
                new Coordinate(15, 22)
            };

            var surroundingLandmarks = new MockSurroundingTileLandmark[3];
            surroundingLandmarks[0] = map.Update(surroundingCoordinates[0], new Coordinate[0]);
            surroundingLandmarks[1] = map.Update(surroundingCoordinates[1], new Coordinate[0]);
            surroundingLandmarks[2] = map.Update(surroundingCoordinates[2], new Coordinate[0]);

            var result = map.Update(new Coordinate(15, 23), surroundingCoordinates);
            Assert.Equal(1, surroundingLandmarks[0].Score);
            Assert.Equal(1, surroundingLandmarks[1].Score);
            Assert.Equal(1, surroundingLandmarks[2].Score);
        }

        [Fact]
        public void CreatesNewLandmarkUsingCountOfSurroundingLandmarks()
        {
            var map = new SurroundingTileLandmarkMap<MockSurroundingTileLandmark>(
                30,
                (count) => new MockSurroundingTileLandmark(count));
            var surroundingCoordinates = new Coordinate[]
            {
                new Coordinate(14, 23),
                new Coordinate(15, 24),
                new Coordinate(15, 22),
                new Coordinate(16, 23)
            };

            map.Update(surroundingCoordinates[0], new Coordinate[0]);
            map.Update(surroundingCoordinates[1], new Coordinate[0]);
            map.Update(surroundingCoordinates[2], new Coordinate[0]);

            var result = map.Update(new Coordinate(15, 23), surroundingCoordinates);
            Assert.Equal(3, result.Score);
        }
    }
}