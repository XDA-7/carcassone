using Carcassone.Engine;
using Xunit;

namespace Carcassone.Engine.Tests_TileMatrix
{
    public class Indexer
    {
        [Fact]
        public void ReturnsTileSetInIndex()
        {
            var tile = new Tile(TileLayouts.DoubleCity);
            var matrix = new TileMatrix(60);
            matrix[10, 45] = tile;
            Assert.Equal(tile, matrix[10, 45]);
        }

        [Fact]
        public void ReturnsBlankTileIfNoTileForIndex()
        {
            var tile = new Tile(TileLayouts.DoubleCity);
            var matrix = new TileMatrix(60);
            matrix[10, 45] = tile;
            AssertTileBlank(matrix[10, 46]);
        }

        private void AssertTileBlank(Tile tile)
        {
            foreach (var edge in tile.GetEdges())
            {
                Assert.Equal(EdgeType.Blank, edge.EdgeType);
            }
        }
    }
}