using System;
using Carcassone.Engine;
using Carcassone.Engine.Tests;
using Xunit;

namespace Carcassone.Engine.Tests_TileMatrix
{
    public class Clone
    {
        [Fact]
        public void TilesAtIdenticalLocationsHaveEqualValues()
        {
            var original = GetTestTileMatrix(5);
            var clone = TileMatrix.Clone(original, 5);
            AssertRelationshipForParallelTiles(original, clone, 5, AssertMatchingTileValues);
        }

        [Fact]
        public void TilesAtIdenticalLocationsAreNotEqualReferences()
        {
            var original = GetTestTileMatrix(5);
            var clone = TileMatrix.Clone(original, 5);
            AssertRelationshipForParallelTiles(original, clone, 5, Assert.NotEqual);
        }

        private TileMatrix GetTestTileMatrix(int size)
        {
            var tileLayouts = TileLayouts.Frequencies.Keys.ToArray();
            var rng = new Random();
            var matrix = new TileMatrix(size);
            for (var i = 0; i < size; i++)
            for (var j = 0; j < size; j++)
            {
                matrix[i, j] = new Tile(tileLayouts[rng.Next(tileLayouts.Length)]);
            }

            return matrix;
        }

        private void AssertRelationshipForParallelTiles(TileMatrix original, TileMatrix clone, int mapSize, Action<Tile, Tile> assertion)
        {
            for (var i = 0; i < mapSize; i++)
            for (var j = 0; j < mapSize; j++)
            {
                assertion(original[i, j], clone[i, j]);
            }
        }

        private void AssertMatchingTileValues(Tile original, Tile clone)
        {
            Assert.Equal(original.Chapel, clone.Chapel);
            Assert.Equal(original.Garden, clone.Garden);
            Assert.Equal(original.Shield, clone.Shield);
            for (var i = 0; i < Tile.TILE_SIDE_COUNT; i++)
            {
                Assert.Equal(original.GetEdge(i).EdgeType, clone.GetEdge(i).EdgeType);
            }
        }
    }
}