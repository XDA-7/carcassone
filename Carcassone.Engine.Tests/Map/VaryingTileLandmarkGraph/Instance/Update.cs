using System.Collections.Generic;
using Carcassone.Engine;
using Carcassone.Engine.Tests;
using Xunit;

namespace Carcassone.Engine.Tests_VaryingTileLandmarkGraph
{
    public class Update
    {
        [Fact]
        public void ReturnsNewLandmarkIfAdjacentEdgesAllBlank()
        {
            var graph = new VaryingTileLandmarkGraph<MockVaryingTileLandmark>(
                () => new MockVaryingTileLandmark());
            var tile = Tile.Blank();
            var tileEdgePairs = new TileEdgePair[]
            {
                new TileEdgePair(tile.GetEdge(0), new TileEdge(EdgeType.Blank)),
                new TileEdgePair(tile.GetEdge(1), new TileEdge(EdgeType.Blank)),
                new TileEdgePair(tile.GetEdge(2), new TileEdge(EdgeType.Blank)),
                new TileEdgePair(tile.GetEdge(3), new TileEdge(EdgeType.Blank))
            };

            var result = graph.Update(tile, tileEdgePairs);
            Assert.Contains(tile, result.GetTiles());
            Assert.Contains(result, graph.GetLandmarks());
        }

        [Fact]
        public void ReturnsAdjacentLandmarkIfOnlyOneExists()
        {
            var graph = new VaryingTileLandmarkGraph<MockVaryingTileLandmark>(
                () => new MockVaryingTileLandmark());
            var tile = Tile.Blank();
            var tileEdgePairs = GetTileEdgePairs(1, 3);
            
            var result = graph.Update(tile, tileEdgePairs);
            Assert.Contains(tile, result.GetTiles());
            Assert.Equal(tileEdgePairs[0].AdjacentEdge.Landmark, result);
        }

        [Fact]
        public void DoesNotAddAdjacentLandmarkToGraphIfOneExists()
        {
            var graph = new VaryingTileLandmarkGraph<MockVaryingTileLandmark>(
                () => new MockVaryingTileLandmark());
            var tile = Tile.Blank();
            var tileEdgePairs = GetTileEdgePairs(1, 3);

            var result = graph.Update(tile, tileEdgePairs);
            Assert.Empty(graph.GetLandmarks());
        }

        [Fact]
        public void ReturnsMergedLandmarkIfMultipleAdjacent()
        {
            var tileEdgePairs = GetTileEdgePairs(3, 1);
            var graph = GetGraphWithLandmarks(
                (MockVaryingTileLandmark)tileEdgePairs[0].AdjacentEdge.Landmark,
                (MockVaryingTileLandmark)tileEdgePairs[1].AdjacentEdge.Landmark,
                (MockVaryingTileLandmark)tileEdgePairs[2].AdjacentEdge.Landmark);
            var tile = Tile.Blank();

            var result = graph.Update(tile, tileEdgePairs);
            Assert.Contains(tile, result.GetTiles());
            Assert.Single(graph.GetLandmarks());
            
        }

        private TileEdgePair[] GetTileEdgePairs(int nonBlank, int blank)
        {
            var result = new List<TileEdgePair>();
            for (var i = 0; i < nonBlank; i++)
            {
                result.Add(new TileEdgePair(new TileEdge(EdgeType.Blank), new TileEdge(EdgeType.Field)));
                result[i].AdjacentEdge.Landmark = new MockVaryingTileLandmark();
            }

            for (var i = 0; i < blank; i++)
            {
                result.Add(new TileEdgePair(new TileEdge(EdgeType.Blank), new TileEdge(EdgeType.Blank)));
            }

            return result.ToArray();
        }

        private VaryingTileLandmarkGraph<MockVaryingTileLandmark> GetGraphWithLandmarks(params MockVaryingTileLandmark[] landmarks)
        {
            var graph = new VaryingTileLandmarkGraph<MockVaryingTileLandmark>(
                () => new MockVaryingTileLandmark());
            var landmarkList = new List<MockVaryingTileLandmark>(landmarks);
            graph.SetPrivateField("_landmarks", landmarkList);
            return graph;
        }
    }
}