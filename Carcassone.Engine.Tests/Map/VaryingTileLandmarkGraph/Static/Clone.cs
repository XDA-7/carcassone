using System.Collections.Generic;
using Carcassone.Engine;
using Carcassone.Engine.Tests;
using Xunit;

namespace Carcassone.Engine.Tests_VaryingTileLandmarkGraph
{
    public class Clone
    {
        [Fact]
        public void CreatesGraphWithPassedLandmarkReferences()
        {
            var landmarks = new MockVaryingTileLandmark[]
            {
                new MockVaryingTileLandmark(),
                new MockVaryingTileLandmark(),
                new MockVaryingTileLandmark(),
                new MockVaryingTileLandmark()
            };

            var clone = VaryingTileLandmarkGraph<MockVaryingTileLandmark>.Clone(
                landmarks,
                () => new MockVaryingTileLandmark());
            var cloneLandmarks = clone.GetPrivateField<VaryingTileLandmarkGraph<MockVaryingTileLandmark>, List<MockVaryingTileLandmark>>("_landmarks");
            Assert.Equal(landmarks.Length, cloneLandmarks.Count);
            foreach (var landmark in landmarks)
            {
                Assert.Contains(landmark, cloneLandmarks);
            }
        }
    }
}