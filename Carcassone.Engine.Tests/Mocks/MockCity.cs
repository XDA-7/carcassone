using Carcassone.Engine;

namespace Carcassone.Engine.Tests
{
    public class MockCity : City
    {
        public bool IsCompleteHook { get; set; }

        public override bool IsComplete() => IsCompleteHook;
    }
}