using System.Collections.Generic;
using Carcassone.Engine;

namespace Carcassone.Engine.Tests
{
    public class MockMap : Map
    {
        public IEnumerable<Coordinate> GetAdjacentCoordinatesValue { get; set; }
        public IEnumerable<Coordinate> GetSurroundingCoordinatesValue { get; set; }
        public IEnumerable<TileEdge> GetAdjacentEdgesValue { get; set; }
        public IEnumerable<TileEdgePair> GetAllAdjacentEdgePairsValue { get; set; }
        public IEnumerable<TileEdgePair> GetAdjacentEdgePairsValue { get; set; }
        public IEnumerable<AdjacentFields> GetAdjacentFieldsValue { get; set; }
        public int GetSurroundingTileCountValue { get; set; }

        public MockMap(int mapSize) : base(mapSize)
        {
            GetAdjacentCoordinatesValue = new Coordinate[0];
            GetSurroundingCoordinatesValue = new Coordinate[0];
            GetAdjacentEdgesValue = new TileEdge[0];
            GetAllAdjacentEdgePairsValue = new TileEdgePair[0];
            GetAdjacentEdgePairsValue = new TileEdgePair[0];
            GetAdjacentFieldsValue = new AdjacentFields[0];
        }

        public override IEnumerable<Coordinate> GetAdjacentCoordinates(int x, int y)
        {
            return GetAdjacentCoordinatesValue;
        }

        public override IEnumerable<Coordinate> GetSurroundingCoordinates(int x, int y)
        {
            return GetSurroundingCoordinatesValue;
        }

        public override IEnumerable<TileEdge> GetAdjacentEdges(int x, int y)
        {
            return GetAdjacentEdgesValue;
        }

        public override IEnumerable<TileEdgePair> GetAllAdjacentEdgePairs(int x, int y)
        {
            return GetAllAdjacentEdgePairsValue;
        }

        public override IEnumerable<TileEdgePair> GetAdjacentEdgePairs(int x, int y, IEnumerable<int> edgeIndices)
        {
            return GetAdjacentEdgePairsValue;
        }

        public override IEnumerable<AdjacentFields> GetAdjacentFields(int x, int y, IEnumerable<int> cornerIndices)
        {
            return GetAdjacentFieldsValue;
        }

        public override int GetSurroundingTileCount(int x, int y)
        {
            return GetSurroundingTileCountValue;
        }
    }
}