using Carcassone.Engine;

namespace Carcassone.Engine.Tests
{
    public class MockSurroundingTileLandmark : SurroundingTileLandmark
    {
        public bool IsCompleteHook { get; set; }

        public MockSurroundingTileLandmark(int initialCount)
        {
            _score = initialCount;
        }

        public override void AddTile()
        {
            _score += 1;
        }

        public override bool IsComplete() => IsCompleteHook;
    }
}