namespace Carcassone.Engine.Tests
{
    public class MockVaryingTileLandmark : VaryingTileLandmark
    {
        protected override void IncrementScore(Tile tile)
        {
            _score += 1;
        }
    }
}