using Carcassone.Engine;
using Carcassone.Engine.Tests;
using Xunit;

namespace Carcassone.Engine.Tests_Tile
{
    public class GetEdgedCorners
    {
        [Fact]
        public void GetsEdgedCornersForStandardTile()
        {
            var tile = new Tile(TileLayouts.TripleCityRoad);
            AssertEdgedCornersCorrect(tile);
        }

        [Fact]
        public void GetsEdgedCornersForBlankTile()
        {
            var tile = Tile.Blank();
            AssertEdgedCornersCorrect(tile);
        }

        private void AssertEdgedCornersCorrect(Tile tile)
        {
            var edgedCorners = tile.GetEdgedCorners(new int[] { 0, 1, 2, 3 }).ToArray();

            var edgedCorner = edgedCorners[0];
            Assert.Equal(tile.GetCorner(0), edgedCorner.TileCorner);
            Assert.Equal(tile.GetEdge(0), edgedCorner.LeftEdge);
            Assert.Equal(tile.GetEdge(1), edgedCorner.RightEdge);

            edgedCorner = edgedCorners[1];
            Assert.Equal(tile.GetCorner(1), edgedCorner.TileCorner);
            Assert.Equal(tile.GetEdge(1), edgedCorner.LeftEdge);
            Assert.Equal(tile.GetEdge(2), edgedCorner.RightEdge);

            edgedCorner = edgedCorners[2];
            Assert.Equal(tile.GetCorner(2), edgedCorner.TileCorner);
            Assert.Equal(tile.GetEdge(2), edgedCorner.LeftEdge);
            Assert.Equal(tile.GetEdge(3), edgedCorner.RightEdge);

            edgedCorner = edgedCorners[3];
            Assert.Equal(tile.GetCorner(3), edgedCorner.TileCorner);
            Assert.Equal(tile.GetEdge(3), edgedCorner.LeftEdge);
            Assert.Equal(tile.GetEdge(0), edgedCorner.RightEdge);
        }
    }
}