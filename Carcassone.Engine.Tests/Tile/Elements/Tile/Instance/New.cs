using Carcassone.Engine;
using Carcassone.Engine.Tests;
using Xunit;

namespace Carcassone.Engine.Tests_Tile
{
    public class New
    {
        [Fact]
        public void Test01()
        {
            var tile = new Tile(TileLayouts.ChapelRoad);
            Assert.Equal("ffrfCgs", tile.Str());
        }

        [Fact]
        public void Test02()
        {
            var tile = new Tile(TileLayouts.TSection.Rotate(1));
            Assert.Equal("rrrfcgs", tile.Str());
        }

        [Fact]
        public void Test03()
        {
            var tile = new Tile(TileLayouts.CityBridgeShield);
            Assert.Equal("fcfccgS", tile.Str());
        }

        [Fact]
        public void Test04()
        {
            var tile = new Tile(TileLayouts.QuestionMarkRight);
            Assert.Equal("crrfcgs", tile.Str());
        }
    }
}