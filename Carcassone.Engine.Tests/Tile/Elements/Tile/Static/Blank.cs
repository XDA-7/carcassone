using Carcassone.Engine;
using Xunit;

namespace Carcassone.Engine.Tests_Tile
{
    public class Blank
    {
        [Fact]
        public void AllEdgesAndCornersAreNotNull()
        {
            var tile = Tile.Blank();
            foreach (var edge in tile.GetEdges())
            {
                Assert.NotNull(edge);
                Assert.Equal(EdgeType.Blank, edge.EdgeType);
            }

            foreach (var corner in tile.GetCorners())
            {
                Assert.NotNull(corner);
            }
        }
    }
}