using Carcassone.Engine;
using Xunit;

namespace Carcassone.Engine.Tests_Tile
{
    public class Clone
    {
        [Fact]
        public void AllEdgeTypesMatch()
        {
            var original = new Tile(TileLayouts.DoubleCity);
            var clone = Tile.Clone(original);
            for (var i = 0; i < 4; i++)
            {
                Assert.Equal(original.GetEdge(i).EdgeType, clone.GetEdge(i).EdgeType);
            }
        }

        [Fact]
        public void AttributesMatch()
        {
            var shieldedTile = new Tile(TileLayouts.DoubleCityShield);
            var shieldedTileClone = Tile.Clone(shieldedTile);
            AssertAttributesEqual(shieldedTile, shieldedTileClone);

            var chapelTile = new Tile(TileLayouts.Chapel);
            var chapelTileClone = Tile.Clone(chapelTile);
            AssertAttributesEqual(chapelTile, chapelTileClone);
        }

        private void AssertAttributesEqual(Tile original, Tile clone)
        {
            Assert.Equal(original.Chapel, clone.Chapel);
            Assert.Equal(original.Garden, clone.Garden);
            Assert.Equal(original.Shield, clone.Shield);
        }

        [Fact]
        public void AllCornersAreNotNull()
        {
            var tile = new Tile(TileLayouts.StraightRoadCity);
            var clone = Tile.Clone(tile);
            foreach (var corner in clone.GetCorners())
            {
                Assert.NotNull(corner);
            }
        }

        [Fact]
        public void AllFieldsAndLandmarksAreNull()
        {
            var tile = new Tile(TileLayouts.StraightRoadCity);
            var city = new City();
            var road = new Road();
            var field1 = new Field();
            var field2 = new Field();
            tile.GetEdge(0).Landmark = city;
            tile.GetEdge(1).Landmark = road;
            tile.GetEdge(3).Landmark = road;
            tile.GetCorner(0).Field = field1;
            tile.GetCorner(1).Field = field2;
            tile.GetCorner(2).Field = field2;
            tile.GetCorner(3).Field = field1;

            var clone = Tile.Clone(tile);
            AssertFieldsAndLandmarksNull(clone);
        }

        private void AssertFieldsAndLandmarksNull(Tile tile)
        {
            foreach (var edge in tile.GetEdges())
            {
                Assert.Null(edge.Landmark);
            }

            foreach (var corner in tile.GetCorners())
            {
                Assert.Null(corner.Field);
            }
        }

        [Fact]
        public void NoReferencesAreShared()
        {
            var tile = new Tile(TileLayouts.ChapelRoad);
            var clone = Tile.Clone(tile);
            foreach (var edge in tile.GetEdges())
            {
                AssertEdgeNotInClone(edge, clone);
            }

            foreach (var corner in tile.GetCorners())
            {
                AssertCornerNotInClone(corner, clone);
            }
        }

        public void AssertEdgeNotInClone(TileEdge tileEdge, Tile clone)
        {
            foreach (var edge in clone.GetEdges())
            {
                Assert.NotEqual(tileEdge, edge);
            }
        }

        public void AssertCornerNotInClone(TileCorner tileCorner, Tile clone)
        {
            foreach (var corner in clone.GetCorners())
            {
                Assert.NotEqual(tileCorner, corner);
            }
        }
    }
}