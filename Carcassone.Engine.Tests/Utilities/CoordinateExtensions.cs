namespace Carcassone.Engine.Tests
{
    public static class CoordinateExtensions
    {
        public static string Str(this Coordinate coordinate) =>
            $"({coordinate.X},{coordinate.Y})";
    }
}