namespace Carcassone.Engine.Tests
{
    public static class EdgeCoordinateExtensions
    {
        public static string Str(this EdgeCoordinate edgeCoordinate) =>
            $"({edgeCoordinate.X},{edgeCoordinate.Y},{edgeCoordinate.Edge})";
    }
}