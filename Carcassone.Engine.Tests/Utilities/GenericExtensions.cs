using System.Reflection;

namespace Carcassone.Engine.Tests
{
    public static class GenericExtensions
    {
        public static void SetPrivateField<T>(this T obj, string fieldName, object value)
        {
            typeof(T).GetField(fieldName, BindingFlags.NonPublic | BindingFlags.Instance)
                .SetValue(obj, value);
        }

        public static U GetPrivateField<T, U>(this T obj, string fieldName)
        {
            return (U)typeof(T).GetField(fieldName, BindingFlags.NonPublic | BindingFlags.Instance)
                .GetValue(obj);
        }
    }
}