using System.Collections.Generic;

namespace Carcassone.Engine.Tests
{
    public static class IEnumerableExtensions
    {
        public static T[] ToArray<T>(this IEnumerable<T> values)
        {
            var list = new List<T>();
            foreach (var value in values)
            {
                list.Add(value);
            }

            return list.ToArray();
        }
    }
}