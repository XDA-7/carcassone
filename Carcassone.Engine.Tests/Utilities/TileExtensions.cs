namespace Carcassone.Engine.Tests
{
    public static class TileExtensions
    {
        public static string Str(this Tile tile)
        {
            var values = new char[7];
            values[0] = EdgeTypeStr(tile.GetEdge(0).EdgeType);
            values[1] = EdgeTypeStr(tile.GetEdge(1).EdgeType);
            values[2] = EdgeTypeStr(tile.GetEdge(2).EdgeType);
            values[3] = EdgeTypeStr(tile.GetEdge(3).EdgeType);
            values[4] = ChapelStr(tile);
            values[5] = GardenStr(tile);
            values[6] = ShieldStr(tile);
            return new string(values);
        }

        private static char EdgeTypeStr(EdgeType edgeType)
        {
            if (edgeType == EdgeType.Blank)
            {
                return 'b';
            }
            else if (edgeType == EdgeType.City)
            {
                return 'c';
            }
            else if (edgeType == EdgeType.Field)
            {
                return 'f';
            }
            else if (edgeType == EdgeType.Road)
            {
                return 'r';
            }
            else
            {
                throw new System.Exception("unknown edge type");
            }
        }

        private static char ChapelStr(Tile tile) => tile.Chapel ? 'C' : 'c';

        private static char GardenStr(Tile tile) => tile.Garden ? 'G' : 'g';

        private static char ShieldStr(Tile tile) => tile.Shield ? 'S' : 's';
    }
}