namespace Carcassone.Engine.Tests
{
    public static class TileLayoutExtensions
    {
        public static string Str(this TileLayout tileLayout)
        {
            var tile = new Tile(tileLayout);
            return tile.Str();
        }
    }
}