using System.Collections.Generic;

namespace Carcassone.Engine
{
    public class FieldClones : LandmarkClones<Field>
    {
        private IEnumerable<CoordinatedTileCorner> _tileCorners;

        public FieldClones(IEnumerable<Field> fields, IEnumerable<Player> players, IEnumerable<CoordinatedTileCorner> tileCorners) : base(fields, players, Field.Clone)
        {
            _tileCorners = tileCorners;
        }

        protected override IEnumerable<CoordinatedLandmark<Field>> GetCoordinatedLandmarks()
        {
            var result = new List<CoordinatedLandmark<Field>>();
            foreach (var tileCorner in _tileCorners)
            {
                var field = tileCorner.TileCorner.Field;
                result.Add(new CoordinatedLandmark<Field>(tileCorner.EdgeCoordinate, field));
            }

            return result;
        }
    }
}