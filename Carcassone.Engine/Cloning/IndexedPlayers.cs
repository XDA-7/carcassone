using System.Collections.Generic;

namespace Carcassone.Engine
{
    public class IndexedPlayers
    {
        private Player[] _players;

        public IndexedPlayers(List<Player> players)
        {
            _players = players.ToArray();
        }

        public Player this[int index]
        {
            get => _players[index];
        }

        public int Length => _players.Length;
    }
}