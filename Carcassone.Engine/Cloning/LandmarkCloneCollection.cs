namespace Carcassone.Engine
{
    public class LandmarkCloneCollection
    {
        public VaryingTileLandmarkClones VaryingTileLandmarkClones { get; }

        public FieldClones FieldClones { get; }

        public LandmarkCloneCollection(VaryingTileLandmarkClones varyingTileLandmarkClones, FieldClones fieldClones)
        {
            VaryingTileLandmarkClones = varyingTileLandmarkClones;
            FieldClones = fieldClones;
        }
    }
}