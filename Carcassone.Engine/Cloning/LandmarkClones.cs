using System;
using System.Collections.Generic;

namespace Carcassone.Engine
{
    public abstract class LandmarkClones<T> where T : Landmark
    {
        private List<CoordinatedLandmark<T>> _coordinatedLandmarks;
        private List<PlayerIndexedLandmark<T>> _playerIndexedLandmarks;
        private List<T> _landmarks;

        public LandmarkClones(IEnumerable<T> landmarks, IEnumerable<Player> players, Func<T, T> cloneFunc)
        {
            var cloneMap = CloneLandmarks(landmarks, cloneFunc);
            _landmarks = new List<T>(cloneMap.Values);
            var coordinatedLandmarks = GetCoordinatedLandmarks();
            AddCoordinatedLandmarks(cloneMap, coordinatedLandmarks);
            AddPlayerIndexedLandmarks(landmarks, players);
        }

        protected abstract IEnumerable<CoordinatedLandmark<T>> GetCoordinatedLandmarks();

        public IEnumerable<CoordinatedLandmark<T>> CoordinatedLandmarks => _coordinatedLandmarks;

        public IEnumerable<T> GetLandmarks() => _landmarks;

        public IEnumerable<U> GetLandmarks<U>() where U : T
        {
            var result = new List<U>();
            foreach (var landmark in _landmarks)
            {
                if (landmark.GetType() == typeof(U))
                {
                    result.Add(landmark as U);
                }
            }

            return result;
        }

        public void SetPlayerClones(IndexedPlayers indexedPlayers)
        {
            foreach (var indexedLandmark in _playerIndexedLandmarks)
            {
                SetPlayerClonesOnLandmark(indexedPlayers, indexedLandmark);
            }
        }

        private void SetPlayerClonesOnLandmark(IndexedPlayers indexedPlayers, PlayerIndexedLandmark<T> indexedLandmark)
        {
            var claimants = GetClaimantsForLandmark(indexedPlayers, indexedLandmark);
            indexedLandmark.Landmark.SetClaimants(claimants);
        }

        private IEnumerable<PlayerClaim> GetClaimantsForLandmark(IndexedPlayers indexedPlayers, PlayerIndexedLandmark<T> indexedLandmark)
        {
            var result = new List<PlayerClaim>();
            for (var i = 0; i < indexedPlayers.Length; i++)
            {
                var claims = indexedLandmark.GetPlayerClaims(i);
                AddClaimant(result, indexedPlayers[i], claims);
            }

            return result;
        }

        private void AddClaimant(List<PlayerClaim> playerClaims, Player player, int claims)
        {
            if (claims != 0)
            {
                playerClaims.Add(new PlayerClaim(player, claims));
            }
        }

        private Dictionary<T, T> CloneLandmarks(IEnumerable<T> landmarks, Func<T, T> cloneFunc)
        {
            var result = new Dictionary<T, T>();
            foreach (var landmark in landmarks)
            {
                result.Add(landmark, cloneFunc(landmark));
            }

            return result;
        }

        private void AddCoordinatedLandmarks(Dictionary<T, T> cloneMap, IEnumerable<CoordinatedLandmark<T>> originalTileEdges)
        {
            _coordinatedLandmarks = new List<CoordinatedLandmark<T>>();
            foreach (var tileEdge in originalTileEdges)
            {
                AddCoordinatedLandmark(cloneMap, tileEdge);
            }
        }

        private void AddCoordinatedLandmark(Dictionary<T, T> cloneMap, CoordinatedLandmark<T> originalLandmark)
        {
            var landmark = originalLandmark.Landmark;
            if (IsLandmarkInClone(landmark))
            {
                var coordinatedLandmark = new CoordinatedLandmark<T>(originalLandmark.EdgeCoordinate, cloneMap[landmark as T]);
                _coordinatedLandmarks.Add(coordinatedLandmark);
            }
        }

        private void AddPlayerIndexedLandmarks(IEnumerable<T> landmarks, IEnumerable<Player> players)
        {
            _playerIndexedLandmarks = new List<PlayerIndexedLandmark<T>>();
            foreach (var landmark in landmarks)
            {
                AddPlayerIndexedLandmark(landmark, players);
            }
        }

        private void AddPlayerIndexedLandmark(T landmark, IEnumerable<Player> players)
        {
            var playerIndexedLandmark = new PlayerIndexedLandmark<T>(landmark);
            var playerIndex = 0;
            foreach (var player in players)
            {
                AddPlayerIndex(playerIndexedLandmark, landmark, player, playerIndex);
            }

            _playerIndexedLandmarks.Add(playerIndexedLandmark);
        }

        private void AddPlayerIndex(PlayerIndexedLandmark<T> playerIndexedLandmark, T landmark, Player player, int index)
        {
            foreach (var claimant in landmark.GetClaimants())
            {
                if (player == claimant.Key)
                {
                    playerIndexedLandmark.AddPlayerIndex(index, claimant.Value);
                    break;
                }
            }
        }

        private bool IsLandmarkInClone(Landmark landmark) => landmark != null && landmark.GetType() == typeof(T);
    }
}