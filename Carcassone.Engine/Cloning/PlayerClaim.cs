namespace Carcassone.Engine
{
    public class PlayerClaim
    {
        public Player Player { get; }

        public int Claims { get; }

        public PlayerClaim(Player player, int claims)
        {
            Player = player;
            Claims = claims;
        }
    }
}