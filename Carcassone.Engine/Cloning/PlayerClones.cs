using System.Collections.Generic;

namespace Carcassone.Engine
{
    public class PlayerClones
    {
        private List<Player> _players;

        public PlayerClones(IEnumerable<Player> players)
        {
            _players = new List<Player>();
            foreach (var player in players)
            {
                _players.Add(Player.Clone(player));
            }
        }

        public IndexedPlayers GetIndexedPlayers() => new IndexedPlayers(_players);
    }
}