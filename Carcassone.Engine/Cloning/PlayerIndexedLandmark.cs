using System.Collections.Generic;

namespace Carcassone.Engine
{
    public class PlayerIndexedLandmark<T> where T : Landmark
    {
        private Dictionary<int, int> _indexedPlayerClaims;

        public T Landmark { get; }

        public PlayerIndexedLandmark(T landmark)
        {
            _indexedPlayerClaims = new Dictionary<int, int>();
            Landmark = landmark;
        }

        public int GetPlayerClaims(int playerIndex)
        {
            if (_indexedPlayerClaims.ContainsKey(playerIndex))
            {
                return _indexedPlayerClaims[playerIndex];
            }
            else
            {
                return 0;
            }
        }

        public void AddPlayerIndex(int playerIndex, int claims) => _indexedPlayerClaims.Add(playerIndex, claims);
    }
}