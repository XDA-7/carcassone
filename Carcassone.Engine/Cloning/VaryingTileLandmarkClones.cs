using System;
using System.Collections.Generic;

namespace Carcassone.Engine
{
    public class VaryingTileLandmarkClones : LandmarkClones<VaryingTileLandmark>
    {
        private IEnumerable<CoordinatedTileEdge> _tileEdges;

        public VaryingTileLandmarkClones(
            IEnumerable<VaryingTileLandmark> landmarks,
            IEnumerable<Player> players,
            IEnumerable<CoordinatedTileEdge> tileEdges) : base(landmarks, players, VaryingTileLandmark.Clone)
        {
            _tileEdges = tileEdges;
        }

        protected override IEnumerable<CoordinatedLandmark<VaryingTileLandmark>> GetCoordinatedLandmarks()
        {
            var result = new List<CoordinatedLandmark<VaryingTileLandmark>>();
            foreach (var tileEdge in _tileEdges)
            {
                AddCoordinatedLandmark(result, tileEdge);
            }

            return result;
        }

        private void AddCoordinatedLandmark(List<CoordinatedLandmark<VaryingTileLandmark>> landmarks, CoordinatedTileEdge tileEdge)
        {
            var landmark = tileEdge.TileEdge.Landmark;
            if (landmark != null)
            {
                landmarks.Add(new CoordinatedLandmark<VaryingTileLandmark>(tileEdge.EdgeCoordinate, landmark));
            }
        }
    }
}