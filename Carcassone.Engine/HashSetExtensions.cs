using System.Collections.Generic;

namespace Carcassone.Engine
{
    public static class HashSetExtensions
    {
        public static T[] ToArray<T>(this HashSet<T> set)
        {
            var result = new T[set.Count];
            set.CopyTo(result);
            return result;
        }
    }
}