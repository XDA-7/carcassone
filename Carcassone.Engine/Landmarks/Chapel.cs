using System;
using System.Collections.Generic;

namespace Carcassone.Engine
{
    public class Chapel : SurroundingTileLandmark
    {
        public static Chapel Clone(Chapel chapel)
        {
            return new Chapel(chapel._score);
        }

        public Chapel(int surroundingTiles): base()
        {
            _score = surroundingTiles + 1;
        }

        public override bool IsComplete() => _score == 9;

        public override void AddTile() => _score += 1;
    }
}