using System.Collections.Generic;

namespace Carcassone.Engine
{
    public class City : VaryingTileLandmark
    {
        public static City Clone(City city)
        {
            return new City(city);
        }

        public City() : base()
        {
        }

        private City(City city) : base(city)
        {
        }

        protected override void IncrementScore(Tile tile)
        {
            if (tile.Shield)
            {
                _score += 2;
            }
            else
            {
                _score += 1;
            }
        }
    }
}