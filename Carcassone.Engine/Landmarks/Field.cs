using System;
using System.Collections.Generic;

namespace Carcassone.Engine
{

    public class Field : Landmark
    {
        public static Field Merge(Field destination, Field source)
        {
            destination.Merge(source);
            return destination;
        }

        public static Field Clone(Field field)
        {
            return new Field(field);
        }
        
        private HashSet<City> _cities;
        private List<TileCorner> _tileCorners;

        public Field(): base()
        {
            InitializeCollections();
        }

        private Field(Field field) : base(field)
        {
            InitializeCollections();
        }

        public IEnumerable<TileCorner> TileCorners
        {
            get { return _tileCorners; }
        }

        public City[] Cities
        {
            get
            {
                var result = new City[_cities.Count];
                _cities.CopyTo(result);
                return result;
            }
        }

        public override bool IsComplete() => false;

        public void AddTile(IEnumerable<EdgedCorner> edgedCorners)
        {
            foreach (var edgedCorner in edgedCorners)
            {
                AddCorner(edgedCorner.TileCorner);
                AddCity(edgedCorner.LeftEdge);
                AddCity(edgedCorner.RightEdge);
            }
        }

        private void AddCorner(TileCorner tileCorner)
        {
            _tileCorners.Add(tileCorner);
            tileCorner.Field = this;
        }

        private void AddCity(TileEdge tileEdge)
        {
            if (tileEdge.EdgeType == EdgeType.City)
            {
                _cities.Add(tileEdge.Landmark as City);
            }
        }

        private void Merge(Field field)
        {
            foreach (var city in field._cities)
            {
                _cities.Add(city);
            }
            foreach (var tileCorner in field._tileCorners)
            {
                _tileCorners.Add(tileCorner);
            }
        }

        public override int Score
        {
            get
            {
                var score = 0;
                foreach (var city in _cities)
                {
                    if (city.IsComplete())
                    {
                        score += 3;
                    }
                }

                return score;
            }
        }

        private void InitializeCollections()
        {
            _cities = new HashSet<City>();
            _tileCorners = new List<TileCorner>();
        }
    }
}