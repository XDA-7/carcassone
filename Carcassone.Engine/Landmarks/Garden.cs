namespace Carcassone.Engine
{
    public class Garden : SurroundingTileLandmark
    {
        public static Garden Clone(Garden garden)
        {
            return new Garden(garden.Score);
        }

        public Garden(int surroundingTiles): base()
        {
            _score = surroundingTiles + 1;
        }

        public override bool IsComplete() => _score == 9;

        public override void AddTile() => _score += 1;
    }
}