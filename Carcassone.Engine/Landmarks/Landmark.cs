using System;
using System.Collections.Generic;

namespace Carcassone.Engine
{
    public abstract class Landmark
    {
        private LandmarkClaimants _claimants;
        protected int _score;

        public Landmark() => _claimants = new LandmarkClaimants();

        protected Landmark(Landmark landmark)
        {
            _score = landmark._score;
        }

        public void Claim(Player player) => _claimants.Claim(player);

        public IEnumerable<KeyValuePair<Player, int>> GetClaimants() => _claimants.GetClaimants();

        public void Finish()
        {
            ScoreWinners();
            _claimants.Finish();
        }

        public void SetClaimants(IEnumerable<PlayerClaim> claimants) => _claimants = LandmarkClaimants.Clone(claimants);

        private void ScoreWinners()
        {
            foreach (var winner in _claimants.Winners())
            {
                winner.Score += Score;
            }
        }

        public bool IsClaimable() => _claimants.IsUnclaimed();

        public abstract bool IsComplete();

        public virtual int Score { get => _score; }

        public void TryFinish()
        {
            if (IsComplete())
            {
                Finish();
            }
        }
    }
}