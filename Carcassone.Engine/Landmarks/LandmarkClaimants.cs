using System.Collections.Generic;

namespace Carcassone.Engine
{
    public class LandmarkClaimants
    {
        public static LandmarkClaimants Clone(IEnumerable<PlayerClaim> claimants)
        {
            var result = new LandmarkClaimants();
            foreach (var claimant in claimants)
            {
                result._meepleCounts.Add(claimant.Player, claimant.Claims);
            }

            return result;
        }

        public static LandmarkClaimants Merge(LandmarkClaimants destination, LandmarkClaimants source)
        {
            destination.Merge(source);
            return destination;
        }

        private Dictionary<Player, int> _meepleCounts;

        public LandmarkClaimants()
        {
            _meepleCounts = new Dictionary<Player, int>();
        }

        public bool IsUnclaimed() => _meepleCounts.Count == 0;

        public void Claim(Player player)
        {
            player.Meeples--;
            _meepleCounts.Add(player, 1);
        }

        public IEnumerable<KeyValuePair<Player, int>> GetClaimants() => _meepleCounts;

        public void Finish()
        {
            foreach (var meepleCount in _meepleCounts)
            {
                meepleCount.Key.Meeples += meepleCount.Value;
            }
        }

        public IEnumerable<Player> Winners()
        {
            var result = new List<Player>();
            var maxVal = 1;
            foreach (var meeple in _meepleCounts)
            {
                if (meeple.Value == maxVal)
                {
                    result.Add(meeple.Key);
                }
                else if (meeple.Value > maxVal)
                {
                    result.Clear();
                    result.Add(meeple.Key);
                    maxVal = meeple.Value;
                }
            }

            return result;
        }

        private void Merge(LandmarkClaimants landmarkClaimants)
        {
            foreach (var claimant in landmarkClaimants._meepleCounts)
            {
                AddClaimant(claimant);
            }
        }

        private void AddClaimant(KeyValuePair<Player, int> claimant)
        {
            if (_meepleCounts.ContainsKey(claimant.Key))
            {
                _meepleCounts[claimant.Key] += claimant.Value;
            }
            else
            {
                _meepleCounts.Add(claimant.Key, claimant.Value);
            }
        }
    }
}