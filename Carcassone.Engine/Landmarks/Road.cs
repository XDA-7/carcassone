using System.Collections.Generic;

namespace Carcassone.Engine
{
    public class Road : VaryingTileLandmark
    {
        public static Road Clone(Road road)
        {
            return new Road(road);
        }

        public Road(): base()
        {
        }

        private Road(Road road) : base(road)
        {
        }

        protected override void IncrementScore(Tile tile) => _score++;
    }
}