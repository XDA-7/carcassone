namespace Carcassone.Engine
{
    public abstract class SurroundingTileLandmark : Landmark
    {
        protected SurroundingTileLandmark() : base()
        {
        }

        public abstract void AddTile();
    }
}