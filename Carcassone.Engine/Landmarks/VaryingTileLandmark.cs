using System;
using System.Collections.Generic;

namespace Carcassone.Engine
{
    public abstract class VaryingTileLandmark : Landmark
    {
        public static VaryingTileLandmark Clone(VaryingTileLandmark landmark)
        {
            var type = landmark.GetType();
            if (type == typeof(City))
            {
                return City.Clone(landmark as City);
            }
            else if (type == typeof(Road))
            {
                return Road.Clone(landmark as Road);
            }
            else
            {
                throw new ArgumentException($"Landmark type {type} not supported");
            }
        }

        public static T Merge<T>(T destination, T source) where T : VaryingTileLandmark
        {
            destination.Merge(source);
            return destination;
        }

        private List<Tile> _tiles;
        protected int _unclosedEdges;

        public int UnclosedEdges { get => _unclosedEdges; }

        public VaryingTileLandmark() : base() => _tiles = new List<Tile>();

        public VaryingTileLandmark(VaryingTileLandmark landmark) : base(landmark)
        {
            _tiles = new List<Tile>();
            _unclosedEdges = landmark._unclosedEdges;
        }

        public override bool IsComplete() => _unclosedEdges == 0;

        protected abstract void IncrementScore(Tile tile);

        public IEnumerable<Tile> GetTiles() => _tiles;

        public void AddTile(Tile tile, IEnumerable<TileEdgePair> tileEdgePairs)
        {
            _tiles.Add(tile);
            foreach (var tileEdgePair in tileEdgePairs)
            {
                AddNewEdges(tileEdgePair.Edge);
                CloseAdjacentEdges(tileEdgePair.AdjacentEdge);
            }
            
            IncrementScore(tile);
        }

        private void Merge(VaryingTileLandmark target)
        {
            MergeTilesAndScore(target._tiles);
            ReassignMergedTiles(target);
            _unclosedEdges += target._unclosedEdges;
        }

        private void AddNewEdges(TileEdge newTileEdge)
        {
            newTileEdge.Landmark = this;
            _unclosedEdges++;
        }

        private void CloseAdjacentEdges(TileEdge adjacentTileEdge)
        {
            if (adjacentTileEdge.EdgeType != EdgeType.Blank)
            {
                _unclosedEdges -= 2;
            }
        }

        private void MergeTilesAndScore(List<Tile> tiles)
        {
            foreach (var tile in tiles)
            {
                if (!_tiles.Contains(tile))
                {
                    _tiles.Add(tile);
                    IncrementScore(tile);
                }
            }
        }

        private void ReassignMergedTiles(VaryingTileLandmark target)
        {
            foreach (var tile in target._tiles)
            {
                foreach (var edge in tile.GetEdges())
                {
                    if (edge.Landmark == target)
                    {
                        edge.Landmark = this;
                    }
                }
            }
        }
    }
}