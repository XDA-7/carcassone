using System.Collections.Generic;

namespace Carcassone.Engine
{
    public class FieldGraph
    {
        public static FieldGraph Clone(IEnumerable<Field> fields)
        {
            var clone = new FieldGraph();
            foreach (var field in fields)
            {
                clone._fields.Add(field);
            }

            return clone;
        }

        private List<Field> _fields;

        public FieldGraph()
        {
            _fields = new List<Field>();
        }

        public Field Update(IEnumerable<EdgedCorner> edgedCorners, IEnumerable<AdjacentFields> adjacentFields)
        {
            var field = GetField(adjacentFields);
            field.AddTile(edgedCorners);
            return field;
        }

        public IEnumerable<Field> GetFields() => null;

        private Field GetField(IEnumerable<AdjacentFields> adjacentFields)
        {
            var uniqueAdjacentFields = GetUniqueAdjacentFields(adjacentFields);
            if (uniqueAdjacentFields.Length == 0)
            {
                return new Field();
            }
            else if (uniqueAdjacentFields.Length == 1)
            {
                return uniqueAdjacentFields[0];
            }
            else
            {
                return MergeFields(uniqueAdjacentFields);
            }
        }

        private Field[] GetUniqueAdjacentFields(IEnumerable<AdjacentFields> adjacentFields)
        {
            var result = new HashSet<Field>();
            foreach (var adjacentField in adjacentFields)
            {
                AddAdjacentFields(result, adjacentField);
            }

            return result.ToArray();
        }

        private void AddAdjacentFields(HashSet<Field> set, AdjacentFields adjacentFields)
        {
            AddAdjacentField(set, adjacentFields.LeftField);
            AddAdjacentField(set, adjacentFields.RightField);
        }

        private void AddAdjacentField(HashSet<Field> set, Field field)
        {
            if (field != null)
            {
                set.Add(field);
            }
        }

        private Field MergeFields(Field[] fields)
        {
            var result = fields[0];
            for (var i = 1; i < fields.Length; i++)
            {
                result = Field.Merge(result, fields[i]);
            }

            return result;
        }
    }
}