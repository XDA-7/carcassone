using System.Collections.Generic;

namespace Carcassone.Engine
{
    public class FourSidedMap : Map
    {
        public static FourSidedMap Clone(
            FourSidedMap map,
            IEnumerable<CoordinatedLandmark<VaryingTileLandmark>> varyingTileLandmarks,
            IEnumerable<CoordinatedLandmark<Field>> fields)
        {
            var clone = new FourSidedMap();
            clone.CloneImpl(map, varyingTileLandmarks, fields);
            return clone;
        }

        public FourSidedMap(int mapSize) : base(mapSize)
        {
        }

        private FourSidedMap() : base()
        {
        }

        public override IEnumerable<Coordinate> GetAdjacentCoordinates(int x, int y)
        {
            return new Coordinate[]
            {
                new Coordinate(x, y + 1),
                new Coordinate(x + 1, y),
                new Coordinate(x, y - 1),
                new Coordinate(x - 1, y)
            };
        }

        public override IEnumerable<Coordinate> GetSurroundingCoordinates(int x, int y)
        {
            return new Coordinate[]
            {
                new Coordinate(x, y + 1),
                new Coordinate(x + 1, y + 1),
                new Coordinate(x + 1, y),
                new Coordinate(x + 1, y - 1),
                new Coordinate(x, y - 1),
                new Coordinate(x - 1, y - 1),
                new Coordinate(x - 1, y),
                new Coordinate(x - 1, y + 1)
            };
        }

        public override IEnumerable<TileEdgePair> GetAllAdjacentEdgePairs(int x, int y) =>
            GetAdjacentEdgePairs(x, y, new int[] { 0, 1, 2, 3 });

        public override IEnumerable<TileEdgePair> GetAdjacentEdgePairs(int x, int y, IEnumerable<int> edgeIndices)
        {
            var centerTile = _tileMatrix[x, y];
            var adjacentEdges = GetAdjacentEdgesAsArray(x, y);
            var result = new List<TileEdgePair>();
            foreach (var edgeIndex in edgeIndices)
            {
                result.Add(new TileEdgePair(centerTile.GetEdge(edgeIndex), adjacentEdges[edgeIndex]));
            }

            return result;
        }

        public override IEnumerable<TileEdge> GetAdjacentEdges(int x, int y) => GetAdjacentEdgesAsArray(x, y);

        private TileEdge[] GetAdjacentEdgesAsArray(int x, int y)
        {
            return new TileEdge[]
            {
                _tileMatrix[x, y + 1].GetEdge(2),
                _tileMatrix[x + 1, y].GetEdge(3),
                _tileMatrix[x, y - 1].GetEdge(0),
                _tileMatrix[x - 1, y].GetEdge(1)
            };
        }

        public override IEnumerable<AdjacentFields> GetAdjacentFields(int x, int y, IEnumerable<int> cornerIndices)
        {
            return new AdjacentFields[]
            {
                new AdjacentFields(GetFieldForCoords(x, y + 1, 2, 1), GetFieldForCoords(x + 1, y, 3, 3)),
                new AdjacentFields(GetFieldForCoords(x + 1, y, 3, 2), GetFieldForCoords(x, y - 1, 0, 0)),
                new AdjacentFields(GetFieldForCoords(x, y - 1, 0, 3), GetFieldForCoords(x - 1, y, 1, 1)),
                new AdjacentFields(GetFieldForCoords(x - 1, y, 1, 0), GetFieldForCoords(x, y + 1, 2, 2))
            };
        }

        public override int GetSurroundingTileCount(int x, int y)
        {
            var result = 0;
            if (_tileMatrix.IsTileBlank(x, y + 1)) result++;
            if (_tileMatrix.IsTileBlank(x + 1, y + 1)) result++;
            if (_tileMatrix.IsTileBlank(x + 1, y)) result++;
            if (_tileMatrix.IsTileBlank(x + 1, y - 1)) result++;
            if (_tileMatrix.IsTileBlank(x, y - 1)) result++;
            if (_tileMatrix.IsTileBlank(x - 1, y - 1)) result++;
            if (_tileMatrix.IsTileBlank(x - 1, y)) result++;
            if (_tileMatrix.IsTileBlank(x - 1, y + 1)) result++;
            return result;
        }

        private Field GetFieldForCoords(int x, int y, int edgeIndex, int cornerIndex)
        {
            var tile = _tileMatrix[x, y];
            return GetFieldForTile(tile, edgeIndex, cornerIndex);
        }

        private Field GetFieldForTile(Tile tile, int edgeIndex, int cornerIndex)
        {
            if (tile.GetEdge(edgeIndex).IsFieldBlocking())
            {
                return null;
            }
            else
            {
                return tile.GetCorner(cornerIndex).Field;
            }
        }
    }
}