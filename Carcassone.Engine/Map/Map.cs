using System.Collections.Generic;

namespace Carcassone.Engine
{
    public abstract class Map
    {   
        private int _mapSize;
        private PlaceableCoordinates _placeableCoordinates;
        protected TileMatrix _tileMatrix;

        public Map(int mapSize)
        {
            _mapSize = mapSize;
            _placeableCoordinates = new PlaceableCoordinates();
            _tileMatrix = new TileMatrix(mapSize);
        }

        protected Map()
        {
        }

        public abstract IEnumerable<Coordinate> GetAdjacentCoordinates(int x, int y);
        public abstract IEnumerable<Coordinate> GetSurroundingCoordinates(int x, int y);
        public abstract IEnumerable<TileEdge> GetAdjacentEdges(int x, int y);
        public abstract IEnumerable<TileEdgePair> GetAllAdjacentEdgePairs(int x, int y);
        public abstract IEnumerable<TileEdgePair> GetAdjacentEdgePairs(int x, int y, IEnumerable<int> edgeIndices);
        public abstract IEnumerable<AdjacentFields> GetAdjacentFields(int x, int y, IEnumerable<int> cornerIndices);
        public abstract int GetSurroundingTileCount(int x, int y);

        public Tile GetTile(int x, int y) => _tileMatrix[x, y];
        public Tile SetTile(TileLayout tileLayout, int x, int y)
        {
            var tile = new Tile(tileLayout);
            _tileMatrix[x, y] = tile;
            var usedCoordinate = new Coordinate(x, y);
            var exposedCoordinates = GetAdjacentCoordinates(x, y);
            _placeableCoordinates.Update(usedCoordinate, exposedCoordinates);
            return tile;
        }

        public IEnumerable<Coordinate> GetPlaceableCoordinates(TileLayout tileLayout)
        {
            var result = new List<Coordinate>();
            foreach (var coordinate in _placeableCoordinates.Get())
            {
                if (CanPlaceTile(tileLayout, coordinate.X, coordinate.Y))
                {
                    result.Add(coordinate);
                }
            }

            return result;
        }

        public IEnumerable<CoordinatedTile> GetCoordinatedTiles()
        {
            var result = new List<CoordinatedTile>();
            foreach (var coordinate in GetCoordinates())
            {
                if (!_tileMatrix.IsTileBlank(coordinate.X, coordinate.Y))
                {
                    result.Add(new CoordinatedTile(coordinate, _tileMatrix[coordinate.X, coordinate.Y]));
                }
            }

            return result;
        }

        public CoordinatedTileFeatures GetCoordinatedTileFeatures()
        {
            var tileCorners = new List<CoordinatedTileCorner>();
            var tileEdges = new List<CoordinatedTileEdge>();
            foreach (var coordinatedTile in GetCoordinatedTiles())
            {
                var coordinate = coordinatedTile.Coordinate;
                var tile = coordinatedTile.Tile;
                for (var i = 0; i < Tile.TILE_SIDE_COUNT; i++)
                {
                    tileCorners.Add(new CoordinatedTileCorner(new EdgeCoordinate(coordinate.X, coordinate.Y, i), tile.GetCorner(i)));
                    tileEdges.Add(new CoordinatedTileEdge(new EdgeCoordinate(coordinate.X, coordinate.Y, i), tile.GetEdge(i)));
                }
            }

            return new CoordinatedTileFeatures(tileEdges, tileCorners);
        }

        public IEnumerable<Coordinate> GetCoordinates()
        {
            var result = new List<Coordinate>();
            for (var i = 0; i < _mapSize; i++)
            {
                for (var j = 0; j < _mapSize; j++)
                {
                    result.Add(new Coordinate(i, j));
                }
            }

            return result;
        }

        public IEnumerable<EdgeCoordinate> GetEdgeCoordinates()
        {
            var result = new List<EdgeCoordinate>();
            for (var i = 0; i < _mapSize; i++)
            {
                for (var j = 0; j < _mapSize; j++)
                {
                    for (var k = 0; k < Tile.TILE_SIDE_COUNT; k++)
                    {
                        result.Add(new EdgeCoordinate(i, j, k));
                    }
                }
            }

            return result;
        }

        private bool CanPlaceTile(TileLayout tileLayout, int x, int y)
        {
            var i = 0;
            foreach (var adjacentEdge in GetAdjacentEdges(x, y))
            {
                if (IsAdjacentEdgeBlocking(tileLayout.Edges[i], adjacentEdge.EdgeType))
                {
                    return false;
                }

                i++;
            }

            return true;
        }

        private bool IsAdjacentEdgeBlocking(EdgeType tileEdge, EdgeType adjacentEdge)
        {
            return adjacentEdge != EdgeType.Blank &&
                tileEdge != adjacentEdge;
        }

        protected void CloneImpl(
            Map map,
            IEnumerable<CoordinatedLandmark<VaryingTileLandmark>> varyingTileLandmarks,
            IEnumerable<CoordinatedLandmark<Field>> fields)
        {
            _mapSize = map._mapSize;
            _placeableCoordinates = PlaceableCoordinates.Clone(map._placeableCoordinates);
            _tileMatrix = TileMatrix.Clone(map._tileMatrix, map._mapSize);
            InitializeLandmarks(varyingTileLandmarks, fields);
        }

        private void InitializeLandmarks(IEnumerable<CoordinatedLandmark<VaryingTileLandmark>> varyingTileLandmarks, IEnumerable<CoordinatedLandmark<Field>> fields)
        {
            InitializeVaryingTileLandmarks(varyingTileLandmarks);
            InitializeFields(fields);
        }

        private void InitializeVaryingTileLandmarks(IEnumerable<CoordinatedLandmark<VaryingTileLandmark>> varyingTileLandmarks)
        {
            foreach (var varyingTileLandmark in varyingTileLandmarks)
            {
                var x = varyingTileLandmark.EdgeCoordinate.X;
                var y = varyingTileLandmark.EdgeCoordinate.Y;
                var edge = varyingTileLandmark.EdgeCoordinate.Edge;
                _tileMatrix[x, y].GetEdge(edge).Landmark = varyingTileLandmark.Landmark;
            }
        }

        private void InitializeFields(IEnumerable<CoordinatedLandmark<Field>> fields)
        {
            foreach (var field in fields)
            {
                var x = field.EdgeCoordinate.X;
                var y = field.EdgeCoordinate.Y;
                var edge = field.EdgeCoordinate.Edge;
                _tileMatrix[x, y].GetCorner(edge).Field = field.Landmark;
            }
        }
    }
}