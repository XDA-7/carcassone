using System.Collections.Generic;

namespace Carcassone.Engine
{
    public class PlaceableCoordinates
    {
        public static PlaceableCoordinates Clone(PlaceableCoordinates placeableCoordinates)
        {
            var clone = new PlaceableCoordinates();
            foreach (var availableCoordinate in placeableCoordinates._availableCoordinates)
            {
                clone._availableCoordinates.Add(availableCoordinate);
            }
            foreach (var usedCoordinate in placeableCoordinates._usedCoordinates)
            {
                clone._usedCoordinates.Add(usedCoordinate);
            }

            return clone;
        }

        private HashSet<Coordinate> _availableCoordinates;
        private HashSet<Coordinate> _usedCoordinates;

        public PlaceableCoordinates()
        {
            _availableCoordinates = new HashSet<Coordinate>(new CoordinateComparer());
            _usedCoordinates = new HashSet<Coordinate>(new CoordinateComparer());
        }

        public void Update(Coordinate usedCoordinate, IEnumerable<Coordinate> exposedCoordinates)
        {
            _usedCoordinates.Add(usedCoordinate);
            _availableCoordinates.Remove(usedCoordinate);
            foreach (var exposedCoordinate in exposedCoordinates)
            {
                if (!_usedCoordinates.Contains(exposedCoordinate))
                {
                    _availableCoordinates.Add(exposedCoordinate);
                }
            }
        }

        public IEnumerable<Coordinate> Get() => _availableCoordinates;
    }
}