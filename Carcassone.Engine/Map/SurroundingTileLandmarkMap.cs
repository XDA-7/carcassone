using System;
using System.Collections.Generic;

namespace Carcassone.Engine
{
    public class SurroundingTileLandmarkMap<T> where T : SurroundingTileLandmark
    {
        public static SurroundingTileLandmarkMap<T> Clone(SurroundingTileLandmarkMap<T> map, Func<T, T> cloneFunc)
        {
            var clone = new SurroundingTileLandmarkMap<T>(map._mapSize, map._factory);
            var coordinatedLandmarks = map.GetCoordinatedLandmarks();
            foreach (var coordinatedLandmark in coordinatedLandmarks)
            {
                clone._map[coordinatedLandmark.EdgeCoordinate.X, coordinatedLandmark.EdgeCoordinate.Y] = coordinatedLandmark.Landmark;
                clone._list.Add(coordinatedLandmark.Landmark);
            }

            return clone;
        }

        private Func<int, T> _factory;
        private T[,] _map;
        private List<T> _list;
        private int _mapSize;

        public SurroundingTileLandmarkMap(int mapSize, Func<int, T> factory)
        {
            _factory = factory;
            _mapSize = mapSize;
            _map = new T[mapSize, mapSize];
            _list = new List<T>();
        }
        
        public IEnumerable<T> GetLandmarks() => _list;

        public T Update(Coordinate centerCoordinate, IEnumerable<Coordinate> surroundingCoordinates)
        {
            IncrementSurroundingTileLandmarks(surroundingCoordinates, out var surroundingCoordinateCount);
            var landmark = _factory(surroundingCoordinateCount);
            _map[centerCoordinate.X, centerCoordinate.Y] = landmark;
            _list.Add(landmark);
            return landmark;
        }

        private void IncrementSurroundingTileLandmarks(IEnumerable<Coordinate> surroundingCoordinates, out int surroundingCoordinateCount)
        {
            surroundingCoordinateCount = 0;
            foreach (var coordinate in surroundingCoordinates)
            {
                var landmark = _map[coordinate.X, coordinate.Y];
                if (landmark != null)
                {
                    landmark.AddTile();
                    surroundingCoordinateCount++;
                }
            }
        }

        private IEnumerable<CoordinatedLandmark<T>> GetCoordinatedLandmarks()
        {
            var result = new List<CoordinatedLandmark<T>>();
            for (var i = 0; i < _mapSize; i++)
            {
                for (var j = 0; j < _mapSize; j++)
                {
                    AddCoordinatedLandmark(result, i, j);
                }
            }

            return result;
        }

        private void AddCoordinatedLandmark(List<CoordinatedLandmark<T>> landmarks, int x, int y)
        {
            var landmark = _map[x, y];
            if (landmark != null)
            {
                landmarks.Add(new CoordinatedLandmark<T>(new EdgeCoordinate(x, y, 0), landmark));
            }
        }
    }
}