namespace Carcassone.Engine
{
    public class TileMatrix
    {
        public static TileMatrix Clone(TileMatrix tileMatrix, int mapSize)
        {
            var clone = new TileMatrix(mapSize);
            for (var i = 0; i < mapSize; i++)
            {
                for (var j = 0; j < mapSize; j++)
                {
                    clone._tiles[i, j] = Tile.Clone(tileMatrix._tiles[i, j]);
                }
            }

            return clone;
        }

        private Tile[,] _tiles;

        public TileMatrix(int mapSize)
        {
            _tiles = new Tile[mapSize, mapSize];
        }

        public Tile this[int x, int y]
        {
            get => GetTile(x, y);
            set { _tiles[x,y] = value; }
        }

        public bool IsTileBlank(int x, int y) => _tiles[x, y] == null;

        private Tile GetTile(int x, int y)
        {
            var tile = _tiles[x, y];
            if (tile != null)
            {
                return tile;
            }
            else
            {
                return Tile.Blank();
            }
        }
    }
}