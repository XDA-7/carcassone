using System;
using System.Collections.Generic;

namespace Carcassone.Engine
{
    public class VaryingTileLandmarkGraph<T> where T : VaryingTileLandmark
    {
        public static VaryingTileLandmarkGraph<T> Clone(IEnumerable<T> landmarks, Func<T> factory)
        {
            var clone = new VaryingTileLandmarkGraph<T>(factory);
            foreach (var landmark in landmarks)
            {
                clone._landmarks.Add(landmark);
            }

            return clone;
        }

        private List<T> _landmarks;
        private Func<T> _factory;

        public VaryingTileLandmarkGraph(Func<T> factory)
        {
            _landmarks = new List<T>();
            _factory = factory;
        }

        public T Update(Tile tile, IEnumerable<TileEdgePair> tileEdgePairs)
        {
            var landmark = GetLandmark(tileEdgePairs);
            landmark.AddTile(tile, tileEdgePairs);
            return landmark;
        }

        public IEnumerable<T> GetLandmarks() => _landmarks;

        private T GetLandmark(IEnumerable<TileEdgePair> tileEdgePairs)
        {
            var adjacentLandmarks = GetAdjacentLandmarks(tileEdgePairs);
            if (adjacentLandmarks.Length == 0)
            {
                return GetNewLandmark();
            }
            else if (adjacentLandmarks.Length == 1)
            {
                return adjacentLandmarks[0];
            }
            else
            {
                return MergeLandmarks(adjacentLandmarks);
            }
        }

        private T GetNewLandmark()
        {
            var result = _factory();
            _landmarks.Add(result);
            return result;
        }

        private T MergeLandmarks(T[] landmarks)
        {
            T result = landmarks[0];
            for (var i = 1; i < landmarks.Length; i++)
            {
                result = VaryingTileLandmark.Merge<T>(result, landmarks[i]);
                _landmarks.Remove(landmarks[i]);
            }

            return result;
        }

        private T[] GetAdjacentLandmarks(IEnumerable<TileEdgePair> tileEdgePairs)
        {
            var result = new HashSet<T>();
            foreach (var tileEdgePair in tileEdgePairs)
            {
                AddAdjacentLandmark(result, tileEdgePair.AdjacentEdge);
            }

            return result.ToArray();
        }

        private void AddAdjacentLandmark(HashSet<T> set, TileEdge tileEdge)
        {
            if (tileEdge.EdgeType != EdgeType.Blank)
            {
                set.Add(tileEdge.Landmark as T);
            }
        }
    }
}