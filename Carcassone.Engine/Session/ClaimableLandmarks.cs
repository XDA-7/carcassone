using System.Collections.Generic;

namespace Carcassone.Engine
{
    public class ClaimableLandmarks
    {
        public City[] Cities { get; set; }
        public Road[] Roads { get; set; }
        public Field[] Fields { get; set; }
        public Chapel Chapel { get; set; }
        public Garden Garden { get; set; }

        public List<Landmark> Landmarks()
        {
            var result = new List<Landmark>();
            result.AddRange(Cities);
            result.AddRange(Roads);
            result.AddRange(Fields);
            if (Chapel != null)
            {
                result.Add(Chapel);
            }

            if (Garden != null)
            {
                result.Add(Garden);
            }

            return result;
        }
    }
}