namespace Carcassone.Engine
{
    public class Player
    {
        public static Player Clone(Player player) => new Player(player);

        public const int INITIAL_MEEPLE_COUNT = 7;
        public const int INITIAL_ABBOT_COUNT = 1;

        public int Meeples { get; set; }
        public int Abbots { get; set; }
        public int Score { get; set; }

        public Player()
        {
            Meeples = INITIAL_MEEPLE_COUNT;
            Abbots = INITIAL_ABBOT_COUNT;
        }

        private Player(Player player)
        {
            Meeples = player.Meeples;
            Abbots = player.Abbots;
            Score = player.Score;
        }

        public bool CanClaim()
        {
            return Meeples != 0;
        }
    }
}