using System;
using System.Collections.Generic;
using System.Linq;

namespace Carcassone.Engine
{
    public class Session
    {
        public const int MAP_SIZE = 60;

        public static Session New(int playerCount = 0)
        {
            var session = new Session(playerCount);
            for (var i = 0; i < playerCount; i++)
            {
                session._players[i] = new Player();
            }

            session.PlaceTile(TileLayouts.StraightRoadCity, MAP_SIZE / 2, MAP_SIZE / 2);
            return session;
        }

        public static Session Clone(Session session)
        {
            var cloneSession = new Session();
            var landmarkClones = GetLandmarkClones(session);
            SetPlayerClones(session, landmarkClones);
            cloneSession._cityGraph = VaryingTileLandmarkGraph<City>.Clone(landmarkClones.VaryingTileLandmarkClones.GetLandmarks<City>(), () => new City());
            cloneSession._roadGraph = VaryingTileLandmarkGraph<Road>.Clone(landmarkClones.VaryingTileLandmarkClones.GetLandmarks<Road>(), () => new Road());
            cloneSession._fieldGraph = FieldGraph.Clone(landmarkClones.FieldClones.GetLandmarks());
            cloneSession._map = FourSidedMap.Clone(
                session._map as FourSidedMap,
                landmarkClones.VaryingTileLandmarkClones.CoordinatedLandmarks,
                landmarkClones.FieldClones.CoordinatedLandmarks);

            cloneSession._chapelMap = SurroundingTileLandmarkMap<Chapel>.Clone(session._chapelMap, Chapel.Clone);
            cloneSession._gardenMap = SurroundingTileLandmarkMap<Garden>.Clone(session._gardenMap, Garden.Clone);

            return cloneSession;
        }

        private static LandmarkCloneCollection GetLandmarkClones(Session session)
        {
            var coordinatedFeatures = session._map.GetCoordinatedTileFeatures();
            var fieldClones = GetFieldClones(session, coordinatedFeatures.TileCorners);
            var varyingTileLandmarkClones = GetVaryingTileLandmarkClones(session, coordinatedFeatures.TileEdges);
            return new LandmarkCloneCollection(varyingTileLandmarkClones, fieldClones);
        }

        private static FieldClones GetFieldClones(Session session, IEnumerable<CoordinatedTileCorner> tileCorners)
        {
            var fields = session._fieldGraph.GetFields();
            return new FieldClones(fields, session._players, tileCorners);
        }

        private static VaryingTileLandmarkClones GetVaryingTileLandmarkClones(Session session, IEnumerable<CoordinatedTileEdge> tileEdges)
        {
            var varyingTileLandmarks = GetVaryingTileLandmarks(session);
            return new VaryingTileLandmarkClones(varyingTileLandmarks, session._players, tileEdges);
        }

        private static void SetPlayerClones(Session originalSession, LandmarkCloneCollection cloneCollection)
        {
            var indexedClones = new PlayerClones(originalSession._players).GetIndexedPlayers();
            cloneCollection.VaryingTileLandmarkClones.SetPlayerClones(indexedClones);
            cloneCollection.FieldClones.SetPlayerClones(indexedClones);
        }

        private static IEnumerable<VaryingTileLandmark> GetVaryingTileLandmarks(Session session)
        {
            var result = new List<VaryingTileLandmark>();
            result.AddRange(session._cityGraph.GetLandmarks());
            result.AddRange(session._roadGraph.GetLandmarks());
            return result;
        }

        private Map _map;
        private Player[] _players;
        private FieldGraph _fieldGraph;
        private VaryingTileLandmarkGraph<City> _cityGraph;
        private VaryingTileLandmarkGraph<Road> _roadGraph;
        private SurroundingTileLandmarkMap<Chapel> _chapelMap;
        private SurroundingTileLandmarkMap<Garden> _gardenMap;

        private Session()
        {
        }

        private Session(int playerCount)
        {
            _map = new FourSidedMap(MAP_SIZE);
            _players = new Player[playerCount];
            _fieldGraph = new FieldGraph();
            _cityGraph = new VaryingTileLandmarkGraph<City>(() => new City());
            _roadGraph = new VaryingTileLandmarkGraph<Road>(() => new Road());
            _chapelMap = new SurroundingTileLandmarkMap<Chapel>(MAP_SIZE, (count) => new Chapel(count));
            _gardenMap = new SurroundingTileLandmarkMap<Garden>(MAP_SIZE, (count) => new Garden(count));
        }

        public void EndTurn()
        {
            foreach (var landmark in GetLandmarks())
            {
                landmark.TryFinish();
            }
        }

        public void Finish()
        {
            foreach (var landmark in GetLandmarks())
            {
                landmark.Finish();
            }
        }

        public IEnumerable<Landmark> GetLandmarks()
        {
            var result = new List<Landmark>();
            result.AddRange(_cityGraph.GetLandmarks());
            result.AddRange(_roadGraph.GetLandmarks());
            result.AddRange(_fieldGraph.GetFields());
            result.AddRange(_chapelMap.GetLandmarks());
            result.AddRange(_gardenMap.GetLandmarks());
            return result;
        }

        public ClaimableLandmarks PlaceTile(TileLayout tileLayout, int x, int y)
        {
            var tile = _map.SetTile(tileLayout, x, y);

            AddVaryingTileLandmarks(x, y, tile, tileLayout, out City[] cities, out Road[] roads);
            var chapel = AddChapel(tile, x, y);
            var garden = AddGarden(tile, x, y);
            var fields = AddFields(tileLayout, tile, x, y);
            return new ClaimableLandmarks
            {
                Cities = cities,
                Roads = roads,
                Fields = fields,
                Chapel = chapel,
                Garden = garden
            };
        }

        public void ScoreLandmarks(ClaimableLandmarks landmarks)
        {
            foreach (var city in landmarks.Cities)
            {
                city.TryFinish();
            }

            foreach (var road in landmarks.Roads)
            {
                road.TryFinish();
            }

            if (landmarks.Chapel != null)
            {
                landmarks.Chapel.TryFinish();
            }

            if (landmarks.Garden != null)
            {
                landmarks.Garden.TryFinish();
            }
        }

        private Chapel AddChapel(Tile tile, int x, int y)
        {
            if (tile.Chapel)
            {
                var surroundingCoordinates = _map.GetSurroundingCoordinates(x, y);
                return _chapelMap.Update(new Coordinate(x, y), surroundingCoordinates);
            }
            else
            {
                return null;
            }
        }

        private Garden AddGarden(Tile tile, int x, int y)
        {
            if (tile.Garden)
            {
                var surroundingCoordinates = _map.GetSurroundingCoordinates(x, y);
                return _gardenMap.Update(new Coordinate(x, y), surroundingCoordinates);
            }
            else
            {
                return null;
            }
        }

        private Field[] AddFields(TileLayout tileLayout, Tile tile, int x, int y)
        {
            var result = new List<Field>();
            foreach (var fieldIndexGroup in tileLayout.GetFieldIndexGroups())
            {
                result.Add(AddField(tile, fieldIndexGroup, x, y));
            }

            return result.ToArray();
        }

        private Field AddField(Tile tile, IEnumerable<int> cornerIndices, int x, int y)
        {
            var edgedCorners = tile.GetEdgedCorners(cornerIndices);
            var adjacentFields = _map.GetAdjacentFields(x, y, cornerIndices);
            return _fieldGraph.Update(edgedCorners, adjacentFields);
        }

        private void AddVaryingTileLandmarks(int x, int y, Tile tile, TileLayout tileLayout, out City[] cities, out Road[] roads)
        {
            cities = AddCities(x, y, tile, tileLayout).ToArray();
            roads = AddRoads(x, y, tile, tileLayout).ToArray();
        }

        private List<City> AddCities(int x, int y, Tile tile, TileLayout tileLayout)
        {
            var result = new List<City>();
            foreach (var cityIndexGroup in tileLayout.GetCityIndexGroups())
            {
                result.Add(AddCity(tile, x, y, cityIndexGroup));
            }

            return result;
        }

        private City AddCity(Tile tile, int x, int y, IEnumerable<int> edgeIndices)
        {
            var adjacentEdgePairs = _map.GetAdjacentEdgePairs(x, y, edgeIndices);
            return _cityGraph.Update(tile, adjacentEdgePairs);
        }

        private List<Road> AddRoads(int x , int y, Tile tile, TileLayout tileLayout)
        {
            var result = new List<Road>();
            foreach (var roadIndexGroup in tileLayout.GetRoadIndexGroups())
            {
                result.Add(AddRoad(tile, x, y, roadIndexGroup));
            }

            return result;
        }

        private Road AddRoad(Tile tile, int x, int y, IEnumerable<int> edgeIndices)
        {
            var adjacentEdgePairs = _map.GetAdjacentEdgePairs(x, y, edgeIndices);
            return _roadGraph.Update(tile, adjacentEdgePairs);
        }
    }

    
}