using System;

namespace Carcassone.Engine
{
    public class TileDispenser
    {
        public TileLayout[] _tileLayouts;
        public int _deckIndex;

        public TileDispenser()
        {
            var deckSize = TileLayouts.DeckSize;
            _tileLayouts = new TileLayout[deckSize];
            _deckIndex = -1;
            InitialiseDeckInOrder();
            ShuffleDeck();
        }

        public TileLayout Next()
        {
            _deckIndex++;
            return _tileLayouts[_deckIndex];
        }

        public int RemainingTiles() => _tileLayouts.Length - _deckIndex - 1;

        private void InitialiseDeckInOrder()
        {
            var deckIndex = 0;
            foreach (var keyValuePair in TileLayouts.Frequencies)
            {
                for (var i = 0; i < keyValuePair.Value; i++)
                {
                    _tileLayouts[deckIndex] = keyValuePair.Key;
                    deckIndex++;
                }
            }
        }

        private void ShuffleDeck()
        {
            var rng = new Random();

            for (var i = _tileLayouts.Length - 1; i > 0; i--)
            {
                var random = rng.Next(i);
                var temp = _tileLayouts[random];
                _tileLayouts[random] = _tileLayouts[i];
                _tileLayouts[i] = temp;
            }
        }
    }
}