using System.Collections.Generic;

namespace Carcassone.Engine
{
    

    public class TileLayout
    {
        private int[][] _fieldIndices;
        private int[][] _cityIndices;
        private int[][] _roadIndices;

        public EdgeType[] Edges { get; }
        public bool Chapel { get; }
        public bool Garden { get; }
        public bool Shield { get; }

        public TileLayout(EdgeType[] edges, bool chapel, bool garden, bool shield, int[][] fieldIndices, int[][] cityIndices, int[][] roadIndices)
        {
            Edges = edges;
            Chapel = chapel;
            Garden = garden;
            Shield = shield;
            _fieldIndices = fieldIndices;
            _cityIndices = cityIndices;
            _roadIndices = roadIndices;
        }

        public TileLayout Rotate(int rotation)
        {
            var rotatedEdges = RotateEdges(rotation);
            var rotatedFieldIndices = RotateLandmarksIndices(_fieldIndices, rotation);
            var rotatedCityIndices = RotateLandmarksIndices(_cityIndices, rotation);
            var rotatedRoadIndices = RotateLandmarksIndices(_roadIndices, rotation);
            return new TileLayout(rotatedEdges, Chapel, Garden, Shield, rotatedFieldIndices, rotatedCityIndices, rotatedRoadIndices);
        }

        public IEnumerable<IEnumerable<int>> GetCityIndexGroups() => _cityIndices;
        public IEnumerable<IEnumerable<int>> GetRoadIndexGroups() => _roadIndices;
        public IEnumerable<IEnumerable<int>> GetFieldIndexGroups() => _fieldIndices;

        private int[][] RotateLandmarksIndices(int[][] landmarksIndices, int rotation)
        {
            var landmarkCount = landmarksIndices.GetLength(0);
            var result = new int[landmarkCount][];
            for (var i = 0; i < landmarkCount; i++)
            {
                result[i] = RotateLandmarkIndices(landmarksIndices[i], rotation);
            }

            return result;
        }

        private int[] RotateLandmarkIndices(int[] landmarkIndices, int rotation)
        {
            var newIndices = new int[landmarkIndices.Length];
            for (var i = 0; i < landmarkIndices.Length; i++)
            {
                newIndices[i] = RotateIndex(i, rotation);
            }

            return newIndices;
        }

        private EdgeType[] RotateEdges(int rotation)
        {
            var rotatedEdges = new EdgeType[Edges.Length];
            for (var i = 0; i < rotatedEdges.Length; i++)
            {
                var newIndex = RotateIndex(i, rotation);
                rotatedEdges[newIndex] = Edges[i];
            }

            return rotatedEdges;
        }

        private int RotateIndex(int index, int rotation)
        {
            return (index + rotation) % 4;
        }
    }
}