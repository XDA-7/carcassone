using System.Collections.Generic;

namespace Carcassone.Engine
{
    public static class TileLayouts
    {
        public readonly static TileLayout AllRoad = new TileLayout(
            edges: new EdgeType[] { EdgeType.Road, EdgeType.Road, EdgeType.Road, EdgeType.Road },
            chapel: false,
            garden: false,
            shield: false,
            fieldIndices: new int[][] { new int[] { 0 }, new int[] { 1 }, new int[] { 2 }, new int[] { 3 } },
            cityIndices: new int[0][],
            roadIndices: new int[][] { new int[] { 0 }, new int[] { 1 }, new int[] { 2 }, new int[] { 3 } }
        );

        public readonly static TileLayout FourCity = new TileLayout(
            edges: new EdgeType[] { EdgeType.City, EdgeType.City, EdgeType.City, EdgeType.City },
            chapel: false,
            garden: false,
            shield: true,
            fieldIndices: new int[0][],
            cityIndices: new int[][] { new int[] { 0, 1, 2, 3 } },
            roadIndices: new int[0][]
        );

        public readonly static TileLayout QuestionMarkLeft = new TileLayout(
            edges: new EdgeType[] { EdgeType.City, EdgeType.Field, EdgeType.Road, EdgeType.Road },
            chapel: false,
            garden: false,
            shield: false,
            fieldIndices: new int[][] { new int[] { 0, 1, 3 }, new int[] { 2 } },
            cityIndices: new int[][] { new int[] { 0 } },
            roadIndices: new int[][] { new int[] { 2, 3 } }
        );

        public readonly static TileLayout QuestionMarkRight = new TileLayout(
            edges: new EdgeType[] { EdgeType.City, EdgeType.Road, EdgeType.Road, EdgeType.Field },
            chapel: false,
            garden: false,
            shield: false,
            fieldIndices: new int[][] { new int[] { 0, 2, 3 }, new int[] { 1 } },
            cityIndices: new int[][] { new int[] { 0 } },
            roadIndices: new int[][] { new int[] { 1, 2 } }
        );

        public readonly static TileLayout SingleCity = new TileLayout(
            edges: new EdgeType[] { EdgeType.City, EdgeType.Field, EdgeType.Field, EdgeType.Field },
            chapel: false,
            garden: false,
            shield: false,
            fieldIndices: new int[][] { new int[] { 0, 1, 2, 3 } },
            cityIndices: new int[][] { new int[] { 0 } },
            roadIndices: new int[0][]
        );

        public readonly static TileLayout DoubleCity = new TileLayout(
            edges: new EdgeType[] { EdgeType.City, EdgeType.City, EdgeType.Field, EdgeType.Field },
            chapel: false,
            garden: false,
            shield: false,
            fieldIndices: new int[][] { new int[] { 0, 1, 2, 3 } },
            cityIndices: new int[][] { new int[] { 0, 1 } },
            roadIndices: new int[0][]
        );

        public readonly static TileLayout DoubleCityShield = new TileLayout(
            edges: new EdgeType[] { EdgeType.City, EdgeType.City, EdgeType.Field, EdgeType.Field },
            chapel: false,
            garden: false,
            shield: true,
            fieldIndices: new int[][] { new int[] { 0, 1, 2, 3 } },
            cityIndices: new int[][] { new int[] { 0, 1 } },
            roadIndices: new int[0][]
        );

        public readonly static TileLayout DoubleCityRoad = new TileLayout(
            edges: new EdgeType[] { EdgeType.City, EdgeType.City, EdgeType.Road, EdgeType.Road },
            chapel: false,
            garden: false,
            shield: false,
            fieldIndices: new int[][] { new int[] { 0, 1, 3 }, new int[] { 2 } },
            cityIndices: new int[][] { new int[] { 0, 1 } },
            roadIndices: new int[][] { new int[] { 2, 3 } }
        );

        public readonly static TileLayout DoubleCityRoadShield = new TileLayout(
            edges: new EdgeType[] { EdgeType.City, EdgeType.City, EdgeType.Road, EdgeType.Road },
            chapel: false,
            garden: false,
            shield: true,
            fieldIndices: new int[][] { new int[] { 0, 1, 3 }, new int[] { 2 } },
            cityIndices: new int[][] { new int[] { 0, 1 } },
            roadIndices: new int[][] { new int[] { 2, 3 } }
        );

        public readonly static TileLayout TripleCity = new TileLayout(
            edges: new EdgeType[] { EdgeType.City, EdgeType.City, EdgeType.Field, EdgeType.City },
            chapel: false,
            garden: false,
            shield: false,
            fieldIndices: new int[][] { new int[] { 1, 2 } },
            cityIndices: new int[][] { new int[] { 0, 1, 3 } },
            roadIndices: new int[0][]
        );

        public readonly static TileLayout TripleCityShield = new TileLayout(
            edges: new EdgeType[] { EdgeType.City, EdgeType.City, EdgeType.Field, EdgeType.City },
            chapel: false,
            garden: false,
            shield: true,
            fieldIndices: new int[][] { new int[] { 1, 2 } },
            cityIndices: new int[][] { new int[] { 0, 1, 3 } },
            roadIndices: new int[0][]
        );

        public readonly static TileLayout TripleCityRoad = new TileLayout(
            edges: new EdgeType[] { EdgeType.City, EdgeType.City, EdgeType.Field, EdgeType.City },
            chapel: false,
            garden: false,
            shield: false,
            fieldIndices: new int[][] { new int[] { 1, 2 } },
            cityIndices: new int[][] { new int[] { 0, 1, 3 } },
            roadIndices: new int[][] { new int[] { 2 } }
        );

        public readonly static TileLayout TripleCityRoadShield = new TileLayout(
            edges: new EdgeType[] { EdgeType.City, EdgeType.City, EdgeType.Field, EdgeType.City },
            chapel: false,
            garden: false,
            shield: true,
            fieldIndices: new int[][] { new int[] { 1, 2 } },
            cityIndices: new int[][] { new int[] { 0, 1, 3 } },
            roadIndices: new int[][] { new int[] { 2 } }
        );

        public readonly static TileLayout CityBridge = new TileLayout(
            edges: new EdgeType[] { EdgeType.Field, EdgeType.City, EdgeType.Field, EdgeType.City },
            chapel: false,
            garden: false,
            shield: false,
            fieldIndices: new int[][] { new int[] { 0, 3 }, new int[] { 1, 2 } },
            cityIndices: new int[][] { new int[] { 1, 3 } },
            roadIndices: new int[0][]
        );

        public readonly static TileLayout CityBridgeShield = new TileLayout(
            edges: new EdgeType[] { EdgeType.Field, EdgeType.City, EdgeType.Field, EdgeType.City },
            chapel: false,
            garden: false,
            shield: true,
            fieldIndices: new int[][] { new int[] { 0, 3 }, new int[] { 1, 2 } },
            cityIndices: new int[][] { new int[] { 1, 3 } },
            roadIndices: new int[0][]
        );

        public readonly static TileLayout OppositeWalls = new TileLayout(
            edges: new EdgeType[] { EdgeType.Field, EdgeType.City, EdgeType.Field, EdgeType.City },
            chapel: false,
            garden: false,
            shield: false,
            fieldIndices: new int[][] { new int[] { 0, 1, 2, 3 } },
            cityIndices: new int[][] { new int[] { 1 }, new int[] { 3 } },
            roadIndices: new int[0][]
        );

        public readonly static TileLayout AdjacentWalls = new TileLayout(
            edges: new EdgeType[] { EdgeType.Field, EdgeType.City, EdgeType.Field, EdgeType.City },
            chapel: false,
            garden: false,
            shield: false,
            fieldIndices: new int[][] { new int[] { 0, 1, 2, 3 } },
            cityIndices: new int[][] { new int[] { 0 }, new int[] { 1 } },
            roadIndices: new int[0][]
        );

        public readonly static TileLayout StraightRoad = new TileLayout(
            edges: new EdgeType[] { EdgeType.Field, EdgeType.Road, EdgeType.Field, EdgeType.Road },
            chapel: false,
            garden: false,
            shield: false,
            fieldIndices: new int[][] { new int[] { 0, 3 }, new int[] { 1, 2 } },
            cityIndices: new int[0][],
            roadIndices: new int[][] { new int[] { 1, 3 } }
        );

        public readonly static TileLayout StraightRoadCity = new TileLayout(
            edges: new EdgeType[] { EdgeType.City, EdgeType.Road, EdgeType.Field, EdgeType.Road },
            chapel: false,
            garden: false,
            shield: false,
            fieldIndices: new int[][] { new int[] { 0, 3 }, new int[] { 1, 2 } },
            cityIndices: new int[][] { new int[] { 0 } },
            roadIndices: new int[][] { new int[] { 1, 3 } }
        );

        public readonly static TileLayout CurvedRoad = new TileLayout(
            edges: new EdgeType[] { EdgeType.Road, EdgeType.Road, EdgeType.Field, EdgeType.Field },
            chapel: false,
            garden: false,
            shield: false,
            fieldIndices: new int[][] { new int[] { 0 }, new int[] { 1, 2, 3 } },
            cityIndices: new int[0][],
            roadIndices: new int[][] { new int[] { 0, 1 } }
        );

        public readonly static TileLayout TSection = new TileLayout(
            edges: new EdgeType[] { EdgeType.Road, EdgeType.Road, EdgeType.Field, EdgeType.Road },
            chapel: false,
            garden: false,
            shield: false,
            fieldIndices: new int[][] { new int[] { 0 }, new int[] { 1, 2 }, new int[] { 3 } },
            cityIndices: new int[0][],
            roadIndices: new int[][] { new int[] { 0 }, new int[] { 1 }, new int[] { 3 } }
        );

        public readonly static TileLayout TSectionCity = new TileLayout(
            edges: new EdgeType[] { EdgeType.City, EdgeType.Road, EdgeType.Road, EdgeType.Road },
            chapel: false,
            garden: false,
            shield: false,
            fieldIndices: new int[][] { new int[] { 0, 3 }, new int[] { 1 }, new int[] { 2 } },
            cityIndices: new int[][] { new int[] { 0 } },
            roadIndices: new int[][] { new int[] { 1 }, new int[] { 2 }, new int[] { 3 } }
        );

        public readonly static TileLayout Chapel = new TileLayout(
            edges: new EdgeType[] { EdgeType.Field, EdgeType.Field, EdgeType.Field, EdgeType.Field },
            chapel: true,
            garden: false,
            shield: false,
            fieldIndices: new int[][] { new int[] { 0, 1, 2, 3 } },
            cityIndices: new int[0][],
            roadIndices: new int[0][]
        );

        public readonly static TileLayout ChapelRoad = new TileLayout(
            edges: new EdgeType[] { EdgeType.Field, EdgeType.Field, EdgeType.Road, EdgeType.Field },
            chapel: true,
            garden: false,
            shield: false,
            fieldIndices: new int[][] { new int[] { 0, 1, 2, 3 } },
            cityIndices: new int[0][],
            roadIndices: new int[][] { new int[] { 2 } }
        );

        // http://russcon.org/RussCon/carcassonne/tiles.html
        public readonly static Dictionary<TileLayout, int> Frequencies = new Dictionary<TileLayout, int>
        {
            { FourCity, 1 },
            { AllRoad, 1 },
            { TripleCity, 3 },
            { TripleCityShield, 1 },
            { TripleCityRoad, 1 },
            { TripleCityRoadShield, 2 },
            { TSection, 4 },
            { CityBridge, 1 },
            { CityBridgeShield, 2 },
            { StraightRoad, 8 },
            { DoubleCity, 3 },
            { DoubleCityShield, 2 },
            { DoubleCityRoad, 3 },
            { DoubleCityRoadShield, 2 },
            { CurvedRoad, 9 },
            { AdjacentWalls, 2 },
            { OppositeWalls, 3 },
            { ChapelRoad, 2 },
            { Chapel, 4 },
            { SingleCity, 5 },
            { QuestionMarkLeft, 3 },
            { QuestionMarkRight, 3 },
            { TSectionCity, 3 },
            { StraightRoadCity, 3 }
        };

        public readonly static int DeckSize = GetDeckSize();

        private static int GetDeckSize()
        {
            var result = 0;
            foreach (var value in Frequencies.Values)
            {
                result += value;
            }

            return result;
        }
    }
}