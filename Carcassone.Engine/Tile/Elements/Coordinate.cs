using System.Collections.Generic;

namespace Carcassone.Engine
{
    public class Coordinate
    {
        public int X { get; }
        public int Y { get; }

        public Coordinate(int x, int y)
        {
            X = x;
            Y = y;
        }

        public Coordinate(Coordinate coordinate)
        {
            X = coordinate.X;
            Y = coordinate.Y;
        }
    }

    public class CoordinateComparer : IEqualityComparer<Coordinate>
    {
        public bool Equals (Coordinate left, Coordinate right)
        {
            return left.X == right.X && left.Y == right.Y;
        }

        public int GetHashCode(Coordinate coordinate)
        {
            return coordinate.X.GetHashCode() ^ coordinate.Y.GetHashCode();
        }
    }
}