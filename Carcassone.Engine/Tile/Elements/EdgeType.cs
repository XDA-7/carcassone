namespace Carcassone.Engine
{
    public enum EdgeType
    {
        Field,
        Road,
        City,
        Blank
    }
}