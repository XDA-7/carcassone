using System.Collections.Generic;

namespace Carcassone.Engine
{
    public class Tile
    {
        public const int TILE_SIDE_COUNT = 4;

        private TileCorner[] _corners;
        private TileEdge[] _edges;
        private bool _chapel;
        private bool _garden;
        private bool _shield;

        public bool Chapel { get => _chapel; }
        public bool Garden { get => _garden; }
        public bool Shield { get => _shield; }

        public static Tile Clone (Tile tile)
        {
            var clone = new Tile();
            clone.InitializeFromTile(tile);
            clone.InitializeTileAttributesFromTile(tile);
            return clone;
        }

        public static Tile Blank()
        {
            var tile = new Tile();
            tile.InitializeBlank();
            return tile;
        }

        public Tile(TileLayout tileLayout)
        {
            InitializeCollections();
            InitializeFromTileLayout(tileLayout);
            InitializeAttributesFromLayout(tileLayout);
        }

        private Tile()
        {
            InitializeCollections();
        }

        public IEnumerable<TileCorner> GetCorners() => _corners;

        public IEnumerable<TileEdge> GetEdges() => _edges;

        public TileEdge GetEdge(int index)
        {
            return _edges[index];
        }

        public TileCorner GetCorner(int index)
        {
            return _corners[index];
        }

        public IEnumerable<EdgedCorner> GetEdgedCorners(IEnumerable<int> cornerIndices)
        {
            var result = new List<EdgedCorner>();
            foreach (var cornerIndex in cornerIndices)
            {
                result.Add(GetEdgedCorner(cornerIndex));
            }

            return result;
        }

        private EdgedCorner GetEdgedCorner(int cornerIndex)
        {
            return new EdgedCorner(
                _corners[cornerIndex],
                GetLeftEdgeForCorner(cornerIndex),
                GetRightEdgeForCorner(cornerIndex)
            );
        }

        private TileEdge GetLeftEdgeForCorner(int cornerIndex) => _edges[cornerIndex];

        private TileEdge GetRightEdgeForCorner(int cornerIndex)
        {
            var edgeIndex = cornerIndex + 1;
            if (edgeIndex == _edges.Length)
            {
                return _edges[0];
            }
            else
            {
                return _edges[edgeIndex];
            }
        }

        private void InitializeCollections()
        {
            _edges = new TileEdge[TILE_SIDE_COUNT];
            _corners = new TileCorner[TILE_SIDE_COUNT];
        }

        private void InitializeFromTileLayout(TileLayout tileLayout)
        {
            for (var i = 0; i < TILE_SIDE_COUNT; i++)
            {
                _edges[i] = new TileEdge(tileLayout.Edges[i]);
                _corners[i] = new TileCorner();
            }
        }

        private void InitializeAttributesFromLayout(TileLayout tileLayout)
        {
            _chapel = tileLayout.Chapel;
            _garden = tileLayout.Garden;
            _shield = tileLayout.Shield;
        }

        private void InitializeFromTile(Tile sourceTile)
        {
            for (var i = 0; i < TILE_SIDE_COUNT; i++)
            {
                _edges[i] = new TileEdge(sourceTile._edges[i].EdgeType);
                _corners[i] = new TileCorner();
            }
        }

        private void InitializeTileAttributesFromTile(Tile sourceTile)
        {
            _chapel = sourceTile.Chapel;
            _garden = sourceTile.Garden;
            _shield = sourceTile.Shield;
        }

        private void InitializeBlank()
        {
            for (var i = 0; i < TILE_SIDE_COUNT; i++)
            {
                _edges[i] = new TileEdge(EdgeType.Blank);
                _corners[i] = new TileCorner();
            }
        }
    }
}