namespace Carcassone.Engine
{
    public class TileEdge
    {
        public EdgeType EdgeType { get; }
        public VaryingTileLandmark Landmark { get; set; }

        public TileEdge(EdgeType edgeType)
        {
            EdgeType = edgeType;
        }

        // The edge prevents the fields in it's adjacent corners from connecting to the adjacent corners along that edge
        public bool IsFieldBlocking() => EdgeType == EdgeType.City || EdgeType == EdgeType.Blank;
    }
}