namespace Carcassone.Engine
{
    public class AdjacentFields
    {
        public Field LeftField { get; }
        public Field RightField { get; }

        public AdjacentFields(Field leftField, Field rightField)
        {
            LeftField = leftField;
            RightField = rightField;
        }
    }
}