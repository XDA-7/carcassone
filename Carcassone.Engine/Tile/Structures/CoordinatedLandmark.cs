namespace Carcassone.Engine
{
    public class CoordinatedLandmark<T> where T : Landmark
    {
        public EdgeCoordinate EdgeCoordinate { get; }

        public T Landmark { get; }

        public CoordinatedLandmark(EdgeCoordinate edgeCoordinate, T landmark)
        {
            EdgeCoordinate = edgeCoordinate;
            Landmark = landmark;
        }
    }
}