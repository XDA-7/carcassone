namespace Carcassone.Engine
{
    public class CoordinatedTile
    {
        public Coordinate Coordinate { get; }
        public Tile Tile { get; }

        public CoordinatedTile(Coordinate coordinate, Tile tile)
        {
            Coordinate = coordinate;
            Tile = tile;
        }
    }
}