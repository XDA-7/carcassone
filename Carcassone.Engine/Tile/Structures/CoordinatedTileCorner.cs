namespace Carcassone.Engine
{
    public class CoordinatedTileCorner
    {
        public EdgeCoordinate EdgeCoordinate { get; }
        public TileCorner TileCorner { get; }

        public CoordinatedTileCorner(EdgeCoordinate edgeCoordinate, TileCorner tileCorner)
        {
            EdgeCoordinate = edgeCoordinate;
            TileCorner = tileCorner;
        }
    }
}