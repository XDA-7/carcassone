namespace Carcassone.Engine
{
    public class CoordinatedTileEdge
    {
        public EdgeCoordinate EdgeCoordinate { get; }
        public TileEdge TileEdge { get; }

        public CoordinatedTileEdge(EdgeCoordinate edgeCoordinate, TileEdge tileEdge)
        {
            EdgeCoordinate = edgeCoordinate;
            TileEdge = tileEdge;
        }
    }
}