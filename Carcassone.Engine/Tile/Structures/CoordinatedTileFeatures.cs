using System.Collections.Generic;

namespace Carcassone.Engine
{
    public class CoordinatedTileFeatures
    {
        public IEnumerable<CoordinatedTileEdge> TileEdges { get; }

        public IEnumerable<CoordinatedTileCorner> TileCorners { get; }

        public CoordinatedTileFeatures(IEnumerable<CoordinatedTileEdge> tileEdges, IEnumerable<CoordinatedTileCorner> tileCorners)
        {
            TileEdges = tileEdges;
            TileCorners = tileCorners;
        }
    }
}