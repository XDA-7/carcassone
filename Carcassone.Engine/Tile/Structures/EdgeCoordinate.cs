namespace Carcassone.Engine
{
    public class EdgeCoordinate
    {
        public int X { get; }
        public int Y { get; }
        public int Edge { get; }

        public EdgeCoordinate(int x, int y, int edge)
        {
            X = x;
            Y = y;
            Edge = edge;
        }
    }
}