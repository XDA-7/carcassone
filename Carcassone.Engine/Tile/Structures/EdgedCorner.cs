namespace Carcassone.Engine
{
    public class EdgedCorner
    {
        public TileCorner TileCorner { get; }
        public TileEdge LeftEdge { get; }
        public TileEdge RightEdge { get; }

        public EdgedCorner(TileCorner tileCorner, TileEdge leftEdge, TileEdge rightEdge)
        {
            TileCorner = tileCorner;
            LeftEdge = leftEdge;
            RightEdge = rightEdge;
        }
    }
}