namespace Carcassone.Engine
{
    public class TileEdgePair
    {
        public TileEdge Edge { get; }
        public TileEdge AdjacentEdge { get; }

        public TileEdgePair(TileEdge edge, TileEdge adjacentEdge)
        {
            Edge = edge;
            AdjacentEdge = adjacentEdge;
        }
    }
}