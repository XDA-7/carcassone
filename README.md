Tasks:
- Investigate chapels/gardens, it seems they aren't scored after initial placing
- Finish Session deep copy constructor, probably have to split landmarks out into specific type lists

Possible optimisations:
- Replace collections allocated per method call with single, reused objects
- Assign initial capacity to lists
- Eliminate virtual calls

Terminology:
- Adjacent: Sharing a tile edge with, each tile has up to 4 adjacent tiles
- Landmark: Any feature of the map which can increase a player's score
- Surrounding: Sharing an edge or corner with, each tile has up to 8 surrounding tiles
- SurroundingTileLandmark: A landmark which is affected by surrounding tiles, ie. chapel or garden
- VaryingTileLandmark: A landmark which can expand by having tiles added to it, ie. city or road.
Although fields technically fit this description, they are treated sparately

Score Averages:

Random: 19
Greedy: 27